% Just a script proving we can use MATLAB to communicate to the ARDUINO. 
% It has no real relevance.

clear all
clc
delete(instrfindall);
arduino=serial('/dev/ttyACM0','BaudRate',9600,'DataBits',8);    % create serial communication object on port COM3
InputBufferSize = 25;
Timeout = 0.1;
arduino.Terminator = '';
set(arduino , 'InputBufferSize', InputBufferSize);
set(arduino , 'Timeout', Timeout);
fopen(arduino); % initiate arduino communication
pause(.1)
%sss=fread(arduino,arduino.BytesAvailable) %//ascii
%fclose(arduino); % end communication with arduino

for i=1:10

    fprintf(arduino,'1>')

    pause(1)
end

fclose(arduino)




