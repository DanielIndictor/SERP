% sampling frequency you used in your experiments
clear
sf= 250; %hz

% first you need to load the data saved in the txt file 
% load blinking
% load eyeOpen

load closed

y1= closed;

% then, you need to "detrend" the data
y=y1-mean(y1)

% fast fourier transformation
Y = fft(y,length(y));
Pyy = Y.*conj(Y)/length(y);
f = sf/(length(y))*(0:length(y)/2+1);

figure(2)
hold on
set(gca, 'YScale', 'log')
plot(f,3*10^12*Pyy(1:length(f)),'k')
title('Power spectral density')
xlabel('Frequency (Hz)')