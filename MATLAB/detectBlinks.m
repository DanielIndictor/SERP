clear plot
clf('reset')

load blinking.mat
load eyeOpen.mat
hold on;

% Method 1 (won't work if no blinks for extended period of time).
% figure(1)
% detrended = blinking-movmean(blinking, 100);
% plot(detrended);
% med=prctile(detrended, 15);
% plot([1:length(detrended)]' .* (detrended < med), detrended .* (detrended < med), '*');

% Method 2: go by absolute differences.
figure(2)
smoothedData = movmean(eyeOpen, 10);
differences = vertcat(0, diff(smoothedData));
dev = std(abs(differences));
plot(smoothedData);
isBlink = (abs(differences) > dev * 1.2);
plot((1:length(smoothedData))' .* isBlink, ...
     smoothedData .* isBlink, '.', 'MarkerSize', 10);

