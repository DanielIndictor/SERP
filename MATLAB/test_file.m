% raw_packets = cast(csvread('raw_packets.csv'), 'uint8');
% 
% sample = raw_packets(1:10, :);
% 
% data = read_data(raw_packets);
% 
% plot(data(:,1))

% Close any leftover connections.
% fclose(instrfindall)

% clear plot
% clear x
i = 1;

% 10 samples per step.
steps = 50;

pause(3)

x = zeros(100*steps, 1);

board = CytonBoard('/dev/ttyUSB0', 1000*43, 24);

tic
% Throw out first empty reading.
board.readChannels(1);

for i = 1:steps
    
  reading = board.readChannels(100);
  beginningIndex = (i-1) * 100 + 1;
  endingIndex = beginningIndex + 100-1;  
  
  x(beginningIndex:endingIndex) = reading(:,1);
  
%   figure(1);
%   hold on;
%   plotx(1:endingIndex), 'm');
end
toc
delete(board)

save x