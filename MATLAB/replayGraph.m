samplingRate = 250;
chunkSize = 25;
chunkWindow = 40;

hold off;
figure(1)
pause(10)

load daniel.mat

data = dominick;

slices = (length(daniel) / chunkSize)

for i = 1:slices
  beginningIndex = (i-1) * chunkSize + 1
  endingIndex = beginningIndex + chunkSize- 1
  
  if i < chunkWindow
      continue
  end
  windowStart = endingIndex - chunkSize*chunkWindow + 1
  plot(data(windowStart:endingIndex))
  pause(chunkSize/samplingRate)
  
end