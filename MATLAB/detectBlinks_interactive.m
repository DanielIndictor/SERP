% raw_packets = cast(csvread('raw_packets.csv'), 'uint8');
% 
% sample = raw_packets(1:10, :);
% 
% data = read_data(raw_packets);
% 
% plot(data(:,1))

% Close any leftover connections.
fclose(instrfindall)

load blinking.mat

clear plot
clf('reset')
clear x
i = 1;


steps = 5000;
% Samples per step.
stepSize = 10;
smoothingSamples = 10; % Amount of nearest values to use in moving average.
% lookBack = 100; % Amount of steps to look back when calibrating minimum
%                % difference necessary to count as an "edge".

% assert(steps > lookBack);              

x = zeros(stepSize * steps, 1);
% standardDev = 0; % Exponentially decaying standard deviation.
differences = zeros(size(x));

board = CytonBoard('/dev/ttyUSB0', (stepSize+5) * steps, 24);
% 
% Throw out first empty reading.
board.readChannels(1)

% How many times we've been in the process of blinking consecutively.
consecutiveBlinkingSamples = 0;
lastSample = 0;
for i = 1:steps
  beginningIndex = (i-1) * stepSize + 1;
  endingIndex = beginningIndex + stepSize- 1;
  
  reading = board.readChannels(stepSize);

  x(beginningIndex:endingIndex) = reading(:,1);

%   pause(stepSize/250);
  
  % Wait to get some data.
  if (i <= lookBack)
      continue
  end
  
  movingAverage = movmean(x(1:endingIndex), smoothingSamples);
  differences = vertcat(0, diff(movingAverage));
  dev = std(abs(differences(end-stepSize*lookBack + 1 : end)));
  
  % Checking if on rising edge of data (eye closing)
  isBlink = ((differences(beginningIndex:endingIndex)) < dev * -1.5);
  
  for edgeSample = 1:length(isBlink)
    if(isBlink(edgeSample))
      consecutiveBlinkingSamples = consecutiveBlinkingSamples + 1; 
    else
      if(consecutiveBlinkingSamples > 7)
        system('echo ''b'' > /dev/ttyACM0');
        disp('Blinked!')
      end
      consecutiveBlinkingSamples = 0;
    end
  end 

%   plot(smoothedData);
%   hold on;
%   plot((1:length(smoothedData))' .* isBlink, ...
%        smoothedData, '.', 'MarkerSize', 10);
%   hold off;
  
end

delete(board)

save x