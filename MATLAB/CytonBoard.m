%
% A pure MatLab interface to talk to the Cyton board. Many of the constants
% are from the documentation here:
% http://docs.openbci.com/Hardware/03-Cyton_Data_Format
%
% Authors: Daniel Indictor, Dominick Villamor
%

classdef CytonBoard
    properties (Constant)
        % Most of the following constants are directly from the
        % documentation.

        BAUD_RATE = 115200;
        % Command characters. 
        MSG_START_STREAM = char('b');
        MSG_END_STREAM = char('s');
        MSG_RESET = char('v');
        % Data Packet Lengths
        PACKET_SIZE = 33;
        % Number of EEG channels in each reading
        N_EEG_READING = 8;
        % Number of auxilliary numbers in each reading. 
        % In this case, it's a 3-axis accelerometer.
        N_AUX_VALUES = 3;

        % Set to .1 because that seems to work, but it is arbitrary.
        TIME_OUT = 1;
    end
    
    properties
        serialObject
        sensorGain
        ready
    end
    
    methods
        function cytonBoard = CytonBoard(comPort, inputBufferSize,  ...
                                         gain)
        
           cytonBoard.sensorGain = gain;
           
           % Set up CytonBoard serial object.

            cytonBoard.serialObject = serial(comPort, ...
                                             'BaudRate', ...
                                             CytonBoard.BAUD_RATE, ...
                                             'DataBits', ...
                                             8);
            set(cytonBoard.serialObject, ...
                'InputBufferSize', inputBufferSize);
            set(cytonBoard.serialObject, ...
                'Timeout', CytonBoard.TIME_OUT);
              
            % Open connection
            fopen(cytonBoard.serialObject);
            % Write a reset
            fwrite(cytonBoard.serialObject, CytonBoard.MSG_RESET, 'uchar');
            % Give board a chance to set itself up.
            pause(.3);
        
            disp('Status Data:');

            % Wait until board sends '$$$', so it can start receiving 
            % commands.
            status_chunk = fread(cytonBoard.serialObject, 3);
            while ~isequal(status_chunk(end-2:end)', '$$$')
                status_chunk = vertcat(status_chunk, ...
                    fread(cytonBoard.serialObject, 1));
            end

            if isequal(status_chunk(1:7)', 'Failure')
                disp('Error. See status:')
                disp(char(status_chunk'))
                
                return;
            end
              
            disp(char(status_chunk'))
            % Let user make sure there were no errors.
            
            fwrite(cytonBoard.serialObject, ...
                   CytonBoard.MSG_START_STREAM, 'uchar');
            cytonBoard.ready = true;
        end
        
        function delete(cytonBoard)
            fwrite(cytonBoard.serialObject, CytonBoard.MSG_END_STREAM, ...
                   'uchar');
            fclose(cytonBoard.serialObject);
        end
        
        function raw_packets = getPackets(cytonBoard, nPackets)
            raw_packets = zeros(nPackets, CytonBoard.PACKET_SIZE, ...
                               'uint8');
                           
            for i = 1:nPackets
                raw_packets(i,:) = fread(cytonBoard.serialObject, ...
                                         CytonBoard.PACKET_SIZE, ...
                                         'uint8')';
            end
        end
       
        function EEGData = readChannels (cytonBoard, nSamples)
            % Re-casted to make room for bit-manipulations.
            raw_packets = cytonBoard.getPackets(nSamples);

            % First convert 24-bit signed int to 32-bit signed int.

            MSB_BYTES = [3 6 9 12 15 18 21 24];

            upper_bytes = cast(raw_packets(:, MSB_BYTES), 'uint32');
            middle_bytes = cast(raw_packets(:, MSB_BYTES + 1), 'uint32');
            lower_bytes = cast(raw_packets(:, MSB_BYTES + 2), 'uint32');


            % Zero out space for a table with 8 columns (one per channel).
            raw_packets_size =  size(raw_packets);
            EEGData = cast(zeros(raw_packets_size(1),8), 'int32');

            EEGData = EEGData +  cast(lower_bytes, 'int32');
            EEGData = EEGData + cast(bitshift(middle_bytes, 8), 'int32');

            upper_bytes_lower_7_bits = bitand(bin2dec('1111111'), ...
                                              upper_bytes);
                                          
            EEGData = EEGData + cast(bitshift(upper_bytes_lower_7_bits, ...
                                              16), ...
                                     'int32');
            
            complements = bitshift(upper_bytes, -7);

            EEGData = EEGData - cast(complements, 'int32') * 2^24;

            % Run final calibration from documentation:
            % Scale Factor (Volts/count) = 4.5 Volts / gain / (2^23 - 1);
            % Where by default the gain is 24x.

            EEGData = cast(EEGData, 'double');
            EEGData = EEGData * (4.5 / cytonBoard.sensorGain / (2^23 - 1));

        end
    end
end