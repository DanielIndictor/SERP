\section{Research Methods \& Analysis}
All methods and processes described were conducted by the student researchers. We used  various scientific and machine learning libraries in Python including Keras, SciPy, and Sklearn \citep{tensorflow2015-whitepaper, scipy, scikit-learn}. The graphical front end is written in PyQt \citep{PyQt}. The N-gram character prediction code was reimplemented in C++ to improve runtime performance. The code we wrote is freely available online at \url{https://gitlab.com/daniel.indictor/SERP} under the GPL license. This repository will continue to update as we make changes and implement more functionality.

\subsection{Blink Semantics}
Blinking produces a consistently shaped signal that can be detected with conventional signal processing tools (see Figure \ref{fig:blink}).

\begin{figure}
    \centering
    \includegraphics[width=11 cm]{blink.png}
    \caption{Voltage over time graph recorded during initial hardware tests. Axes are not present as we did not implement scaling techniques in initial hardware tests. Dips in the graph correspond to blinks by the researcher for a total of 3 blinks in the above image.}
    \label{fig:blink}
\end{figure}

The presence of a blink in a certain duration of time can be described as a blink event and the absence of a blink can be described as a non-blink event.
A blink functions as a pushing event: it highlights the next field or button available on the VK.
The absence of a blink during some pre-defined duration of time selects the character which the highlighted field is over.
This duration is called the timestep duration, or the period of time in which an option is available for selection via a blink event. 
Timestep duration is further split up between ``normal'' and ``extended'' timers.
The extended timer allows the user more time to adjust to changes in the linear grid by allowing more wait time on the first button.
We increased the timer length on this button to allow for the participant-researcher to be able to identify the position of a target character and the corresponding sequence of blink and non-blink events to reach it in hopes of decreasing the number of mistakes made.
These unique timers therefore optimized the timestep duration by providing more time in situations it was needed rather than using one du.

% While real-world figures exist for typical timestep durations, they are generally tailored to the subjects being tested. We dedicated a calibration and training session to determine the optimal timestep duration for each of the VKs for our reearcher.
While there are preexisting figures for typical timestep durations, they are generally tailored to the subjects being tested. We dedicated a calibration and training session to determine the optimal timestep duration for each of the VKs for our reearcher.

We assigned this at 0.5 seconds as a starting point. Initial tests to ensure basic functionality of the VKs proved this starting figure for timestep duration to be too short and it was instead set to 2 seconds. Timestep duration was gradually decreased until the researcher-participant was unable to comfortably type thereby maximizing the speed in real world performance tests. Calibration took inspiration from methods used by \cite{Roark2010} in which timestep duration decreased by 100ms until we observed 3 errors in typing a target sentence upon which timestep duration increased by 50ms until no errors were observed. The timestep duration for the linear grid was ultimately lowered to 1.5 seconds and the extended timestep duration was set at 2 seconds. The timestep duration for the T9 was lowered to 1.25 seconds and no extended timestep duration was used: the timestep duration for the backspace button was also 1.25 seconds.

\subsection{Linear Grid}
% The linear grid is the simplest keyboard layout in terms of complexity. 30 buttons are presented on a 6-by-5 grid with the last button being the backspace ($\leftarrow$) button (see Figure \ref{fig:linear}). At each timestep the next letter is highlighted for selection, and a blink event at some timestep will place that character into the text field. The order of the 29 typable characters depends on the language model being used. Figure \ref{fig:linear} presents the characters in alpabetical order trailed by the space character and punctuation characters.

The linear grid is the simplest keyboard layout in terms of complexity of usage. 30 buttons are presented on a 6-by-5 grid with the first button being the backspace button (see Figure \ref{fig:linear}). At each timestep the next character is highlighted for selection. If a blink is detected, the highlighted field pushes to the next character. If a blink event is not detected, then that character is placed into the text field. 
The order of the 29 typable characters depends on the language model being used and their predictions. This order refreshes after a character is selected. Figure \ref{fig:linear} presents the characters in alpabetical order trailed by the space character and punctuation characters.

\begin{figure}[h]
    \centering
    \includegraphics{linear_grid.png}
    \caption{Linear grid for use in experimentation. The target sentence to be typed is copy-pasted into the top textbox. The textbox under it reflects the characters typed so far. The diamond shape ``$\Diamond$'' is used as a substitute for the space character to make it easy for the user to quickly identify extraneous spaces. Here the letter ``r'' is the current candidate for selection. The numbers under the characters represent the number of blinks needed to reach that character starting from the backspace key. Note the colors were inverted to improve readability on printed paper.
   }
    \label{fig:linear}
    
\end{figure}

\subsubsection{8-gram Language Model}

N-gram language models have well-known uses in text prediction.
For example, if an 8-gram is presented with the characters ``adjacen'', a simple model can predict that the next letter is ``t'' by taking the amount of times that the characters ``adjacen'' are followed by ``t'' in some training text and dividing that by the amount of times that the characters ``adjacen'' are followed by any character.
This is called Maximum Likelihood estimation and can be expressed mathematically as follows:
$$p_\textrm{ML}(w_i | w_{i-(n-1)}^{i-1}) = \frac{c(w^i_{i-(n-1)})}{c(w^{i-1}_{i-(n-1)})}$$
where $w_i$ is the $i$th character in a sequence of characters $w$ and $w_a^b$ represents the letters from $w_a$ to $w_b$, inclusive, and where $c(w_a^b)$ represents the amount of times that the sequence $w_a^b$ appears in $w$. This model breaks down when it encounters sequences of text it has never seen before. Say, for example, that the model encounters the characters ``assaile'' but has never seen the word ``assailed''. It would incorrectly assign 0 to be the probability that the letter ``d'' comes next. While many methods exist to solve this zero-frequency problem to varying degrees of success, we chose Witten-Bell smoothing \citep{Witten1991} just as \cite{Roark2010} did. The equations for the Witten-Bell probability were adapted from \cite{Sokolov2015} and are shown below:
\begin{center}\begin{align*}
    p_\textrm{WB}(w_i | w_{i-(n-1)}^{i-1}) &= \lambda_{w_{i-(n-1)}^{i-1}} p_\textrm{ML}(w_i | w_{i-(n-1)}^{i-1}) + (1-\lambda_{w_{i-(n-1)}^{i-1}}) p_\textrm{WB}(w_i | w_{i-(n-1) + 1}^{i-1})\\
    \text{where } \lambda_{w_{i-(n-1)}^{i-1}} &= 1 - \frac { N _ { 1 + } (w_{i-(n-1)}^{i-1}) } { N _ { 1 + } (w_{i-(n-1)}^{i-1}) + \sum_{w_i} c(w_{i-(n-1)}^{i})} \\
    \text{and } N_{1+} (w_a^b)  &= \left| \{ w _ {b+1} : c(w_{a}^{b+1}) > 0 \} \right| \\
\end{align*}\end{center}
The recursion was grounded with the Maximum Likelihood model when $n=1$.

\subsubsection{RNN Language Model}

We framed the character prediction for the RNN as a regression problem: given some character, the network was tasked with predicting the next character.
However, characters cannot be fed directly into neural networks.
Instead, they must first be encoded with one-hot encoding.
Since there are 29 different possible characters (26 different letters as well as periods, commas, and spaces), each character is represented by a vector of size 29 where each position in the vector corresponds to a different character and a 1 at some position indicates that the vector represents that character.
For example, a vector of 28 zeros followed by a one corresponds to ``z'', the last character in the alphabet.
Next, a 2-dimensional array of vectors representing different letters is constructed to form a sentence.
This array is then fed to the RNN as the target value, or the y-value.
The corresponding x-value is the same as the input except shifted to the right by one.
For example, to predict the sentence ``the cat sat on the mat.'', the network would be fed ``.the cat sat on the mat'' (the period in the beginning of the input indicates the beginning of a new sentence).
This means that when the sixth character from the input is ``c'', the network target is the sixth letter of the original sentence, or ``a''.
We chose to use two long short-term memory (LSTM) layers with 1000 and 100 neurons because networks of similar size and structure have shown promise in word-level n-grams.
The output layer is a simple dense layer with 29 nodes limited to outputs between 0 and 1 with the sigmoid activation function, where an output of one in some position indicates that the network is certain that that corresponding character will follow the input.
We used the Adam optimizer to minimize the cross entropy (a metric describing the ``wrongness'' of the model's prediction).

Additionally, we trained a model that uses an input vector of 58 elements: 29 for the input character, and another 29 for the Witten-Bell model's predictions.
This means that this RNN is able to draw upon the probabilities of the incoming letters as assigned by the Witten-Bell smoothed n-gram model.
We called this the Combined RNN because, theoretically, it can combine the strengths of the RNN and the n-gram models. 

\subsubsection{Training}
To train the RNN-based and 8-gram language models, we used the Brown corpus.
We prepared the corpus before training by making all characters lowercase, omitting any characters that were not letters, commas, periods, or spaces, and replacing all terminal punctuation marks with periods.
The corpus was divided into sections: training, testing, and for the RNNs, validation.
We used 40,000 sentences for training the 8-gram. For the RNNs, the training data was further split into training (36,000) and validation (4,000) data as neural networks require validation data sets to ensure they do not overfit the training data. We trained the model for a total of 10 epochs.
The remaining data from the corpus, numbering 13,227 sentences, was used for simulated benchmarks to determine the efficacy of the models using the aforementioned bits per character metric.

\subsection{T9}
The operation of the T9-like virtual keyboard was designed to be simple and faithful to the classic design found on older cell phones. Figure \ref{fig:T9} shows the layout of the keys on the grid. The procedure to select a key on that grid is as follows: first, a horizontal line will scan vertically, highlighting all of the keys on a given row for the duration of a single time-step. When the program detects a blink event, it begins scanning through the keys within the selected row. A second blink event will trigger the specific key highlighted. If the key is not ``1'', then the corresponding number is entered into a buffer area (Figure \ref{fig:T9}) and the highlighting restarts at the first row, scolling down vertically. As more numbers are typed, the virtual keyboard provides a preview of the available words that may be represented by the numbers. For example, the sequence ``2665'' may be intended to represent the word ``book'' while it also corresponds to the word ``cool''. To select a specific word, key 1 must be selected to cycle through the available words, dwelling on each possible word for the same timestep duration. To type a comma, key 1 may be selected once while there are no letters in the buffer. To type a period, key 1 may be selected twice. To erase the last typed word, key 1 may be selected 3 times in a row.

\subsubsection{Training}
The T9 text prediction was trained on the words in the American Aspell dictionary with uppercase letters converted to lowercase \citep{Dictionary}. When testing, words occuring in the test set that were not in the dictionary were ignored in the average.

\subsection{Binary Code Length and Metrics}

% Our metric for evaluating the speed of our virtual keyboards in a simulated setting is bits per character. This describes the number of blink and non-blink events, or the number of yes/no decisions made by the typist, necessary to input a character on average. The number of bits for any character is equal to its binary code length. For example if the letters were laid out in alphabetical order on a linear grid, typing the word ``cab'' would require 3 bits to enter the letter ``C'' (0/no for A, 0/no for B, 1/yes for C), 2 bits for the letter ``B'', and 1 bit for the letter ``A''. This means that for an alphabetically-arranged linear grid, the word ``cab'' requires on average 2 bits per character to type. 

Our metric for evaluating the speed of our virtual keyboards in a simulated setting is bits per character. This describes the number of blink and non-blink events, or the number of yes/no decisions made by the typist, necessary to input a character on average. The number of bits for any character is equal to its binary code length. For example if the letters were laid out in alphabetical order on a linear grid, typing the word ``cab'' would require 4 bits to enter the letter ``C'' (0/no for backspace, 0/no for A, 0/no for B, 1/yes for C), 3 bits for the letter ``B'', and 2 bit for the letter ``A''. This means that for an alphabetically-arranged linear grid, the word ``cab'' requires on average 4.5 bits per character to type. 

\subsection{Simulated Application}
\subsubsection{Results}
After 10 epochs we determined that the regular RNN reached a point of diminishing returns where it neither overfit the training set nor did it substantially improve over the ninth epoch. The compound RNN reached this point after 9 epochs of training. 
Our simulated testing concluded that the 8-gram model was the fastest at 3.65 bits per character, followed by both RNNs which achieved 3.88 bits per character with the T9 being the slowest at 4.97 bits per character (See Figure \ref{fig:simulated_benchmark}). 

\begin{figure}
    \centering
    \includegraphics[resolution=600]{simulated_benchmarks.png}
    \caption{Results from simulated benchmarks. Lower scores indicate that the keyboard required less wait time.}
    \label{fig:simulated_benchmark}
\end{figure}

\subsubsection{Discussion}
Since the last revision of this paper, the VKs appear slower by the bits per character metric due to a change in the layout which saw the backspace placed at the beginning of the linear grid. While we believe this to be advantageous in real world testing, in simulated testing it ended up increasing the bits per character metric by one because it takes one bit to pass over the backspace character when selecting any key. 

Our 8-gram performed comparably to the one used in \citet{Roark2010} which achieved a simulated speed of 3.4 bits per character assuming perfect (no errors while typing) operation while ours achieved a simulated speed of 2.65 under the same conditions. The performance difference can be attributed to the fact that our linear grid did not have numbers, which would be very hard to predict, as well as the fact that we used a different corpus. 

Neither RNN outperformed the 8-gram, contrary to our hypothesis. We will continue experimenting with different network structures to see if they can outperform the 8-gram.

% The RNNs' worse performance may have been due to the fact that they were designed to predict a single character as following a prior sequence of characters. The RNN thus sought to learn in such a way that determined the likelihood that the next most likely characters as well as any other character to be as low as possible. 
% Further testing shows the combined RNN does not outperform the 8-gram nor the RNN despite lower loss values.

The T9-like text prediction's speed was 4.97 characters per minute. Due to the limitations of the T9 layout, a minimum of 4 bits are required to type any character since it takes one blink to pass the first row, one non-blink to select the second row, and one blink and one non-blink to select the second button in the second row which is the first non-punctuation key. Another limitation of the T9 text prediction was that it is only able to predict words within its dictionary. During testing, the T9 encountered 5,272 instances of words not in the dictionary, 3,308 of which were unique. These out-of-dictionary words included some proper nouns and honorifics such as ``Mr'' and ``Mrs''. This limitation depends entirely on the corpus and dictionary used which are relatively easy to change due to the modular nature of the code.

\subsection{Real World Application}
We devised a functional experimental setup for our real world testing which detected blinks by the researcher.
To record EOG signals, we used the biosensing kit offered by OpenBCI including electrodes, headset, and data acquisition board. It is markedly less expensive than medical grade solutions (\euro{}800 vs. \euro{}20,000) despite having comparable hardware capabilities \citep{Frey2016}.
We cleaned the surfaces coming into contact with the participant with 91\% rubbing alcohol pads. 
We then connected electrodes to the forehead and earlobes via the headset and plastic clips, respectively.
Electrodes were placed according to the 10--20 system for electrode placement: a forehead electrode at Fp1, a reference electrode at A1, or the left earlobe, and a ground at A2, or the right earlobe.
Once the participant/researcher was connected to the headset, we used the voltage difference between the forehead and one ear electrode (reference) to find blinks, while the other ear electrode (ground) was used by the data acquisition board to attentuate powerline noise.
Blinks are detected using a Python program that compares the voltage readings of consecutive samples to determine if the voltage falls then rises, a behavior determined to correspond with a blink. 
The participant-researcher typed 10 sentences, selected from the testing set of the corpus, with two of the four VKs: the 8-gram and T9-like system. These sentences can be found in Appendix \ref{appendix:sentences}. Real world testing for the RNN VKs is ongoing. As the corpus was prepared for language model training beforehand, some sentences chosen for real world testing were rejected as they did not make sense when read, indicating the removal of characters such as quotation marks during processing.

We used the characters per minute metric for real world testing instead of bits per character because as we use it, it does not account for user error. Characters per minute is measured by dividing the total number of characters in the 10 sentences of the training set by the time, in minutes, needed to type them for each virtual keyboard.

\subsubsection{Results}
% \begin{table}[h]
%     \centering
%         \begin{tabular}{lr}\toprule
%         Virtual Keyboard & Speed (characters per minute) \\ \midrule
%         8-Gram & \\
%         RNN & \\
%         Combined RNN & \\
%         T9-like & \\ \bottomrule
%         \end{tabular}
%     \caption{Results from real world benchmarks. Higher scores indicate greater speed.}
%         \label{tab:1}
% \end{table}
%Table is here but histogram is better

\begin{figure}[H]
    \centering
    \includegraphics[resolution=600]{experimental_results.png}
    \caption{Results from experiments. The Virtual Keyboard marked "Contemporary" represents the typing speed achieved by \cite{Teja2015}.}
    \label{fig:experimental_results}
\end{figure}
Our experimental results are described in Figure \ref{fig:experimental_results}. Given the average length of words in the English language is approximately 4.5 letters, our keyboards translate to approximately 2.6--4.6 words per minute.
%WPM THINGY OVER HERE TOO
\subsubsection{Discussion}

The best performing VK was the RNN linear grid despite the 8-gram model performing best in simulated testing.
The researcher-participant tested on the RNN linear grid after the other linear grid VKs and may have become more proficient with this type of VK over the course of experimentation. This may explain the increased speed observed with the Recurrent Neural Network despite it not being the highest performing language model.

Both the 8-gram linear grid and T9-like system vastly outperformed contemporary VKs (5 characters-per-minute set by \citet{Teja2015}), achieving over triple and double that speed, respectively. This proves that text prediction is a worthwhile addition to VKs and that the act of blinking is a reliable way to control them. To our knowledge the linear grid configuration with language modeling constitutes the fastest blink-based keyboard available. While faster virtual keyboards exist, they typically require extended training time while our method requires minimum calibration. This suggests that these virtual keyboards may have applications in aiding paralyzed patients as temporary, low-cost solutions.

\paragraph{Observations on Practicality} This section outlines qualitative observations that the researchers made that are relevant when considering usage outside of a scientific setting.

We made layout changes before testing to improve the user experience. During pre-testing calibration, we determined that it was best to place the backspace key at the beginning of the linear grid to make it easy to access; the initial configuration had the backspace key at the very end, meaning any mistakes made during typing required a significant amount of time and significant number of blinks to undo. While the backspace's new position significantly decreased the speed in simulated testing (increasing the bits-per-character by one), its effect is harder to quantify in real world testing as a single bit can take between one tenth of a second \citep{Schiffman2001}---the average time needed to blink---and 1.5 seconds, the ``normal'' timestep duration in our experiment.

One aspect of evaluating the practicality of a VK is how difficult it is to operate and how quickly the user can become comfortable typing on it. The linear grid VK utilizing the 8-gram was more fatiguing as characters could be placed as many as 29 blinks away while on the T9-like system the maximum number of blinks to reach the furthest button was five. We found that the linear grid was simpler and thus easier to use since blink events and non-blink events were used only to change the highlighted character and select it, respectively. By contrast, blinks on the T9-like layout could be used to select rows, select numbers, and change the prediction of the T9-like layout. 

We found that the numbers under buttons marking their distance from the backspace key were helpful when typing characters further down the grid because they allowed the participant-researcher locate the target character and simply count out the number of blinks needed to reach the target character instead of visually verifying the position of the highlighted character and coordinating the number of blinks needed ( see Figure \ref{fig:linear}). 

The extended timestep duration for the first key was also helpful because it gave the participant-researcher more time to find their target character without extending the timestep duration for selecting it. This extended duration was specifically used on the T9-like layout to ``map out'' which numbers contained target letters and the corresponding blinking decisions and on the linear grid to identify the position of the target character.

Though the focus of our study was to evaluate the effect of various text prediction methods on the speed of virtual keyboards, we made several design decisions that make them more viable for use by people with LIS and people with similar extents of paralysis in comparison with other VKs from our literature. Most notably, the input method used by \cite{Teja2015} is not viable for use by people with LIS because it requires vertical \textit{and} horizontal movement of the eyes to move whereas our input method is viable since our VKs solely use vertical eye movement. 

Though several virtual keyboards---especially those utilizing biosignals such as electroencephalograms (EEG)---achieve higher speeds, our proposed VKs require less expensive hardware.
%{\Huge DO THE NAPKIN MATH CHARACTERS/MINUTE/DOLLARS)}.
For example, the keyboard presented by Nagel \& Sp\"uler, to our knowledge the fastest virtual keyboard viable for use by people with LIS, utilizes a hardware system comprising of ``g.USBamp (g.tec,  Austria)  EEG  amplifier,  three  personal  computers  (PCs), Brainproducts Acticap system with 32 channels and a LCD monitor (BenQ XL2430-B) for stimuli presentation'' (\citeyear{Nagel}). The cost of the amplifier alone costs over 11,350 EUR \citep{Gtec}, making the overall cost of this system vastly exceed our proposed system which costs around 1000 USD. The figure provided for the cost of our system represents its current configuration which exceeds the hardware capabilities necessary to run the virtual keyboard. We estimate using different components our virtual keyboard can cost around 500 USD.

\section{Conclusion}
We present EOG-enabled virtual keyboards which utilize blinking and various text prediction methods to create a low-cost, easy-to-use keyboard for paralyzed individuals such as those with LIS. Our highest performing keyboard exceeded the speed of a similar VK presented by \cite{Teja2015} by a factor of 4, constituting the fastest blink-based virtual keyboard viable for use by paralyzed individuals, at a cost significantly lower than other faster virtual keyboards viable for use by those individuals. 
