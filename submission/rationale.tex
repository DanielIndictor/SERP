\subsection{Neurogenerative Disorders}
Locked-in syndrome (LIS) is a neuro-degenerative disorder which leads to loss of motor function \citep{NINDS}.
While the extent of the loss varies, the ``classic'' case of LIS leaves patients fully paralyzed with only the ability to control their vertical eye movement and blinking \citep{Bauer1979}. 
Communication by typing is therefore exceedingly difficult \citep{NINDS}.
Similar complications are seen in other neurodegenerative disorders such as amyotrophic lateral sclerosis and Guillain-Barr\'e syndrome \citep{ALS,GBS}. 

% Human-Computer Interfaces (HCIs) are devices which facilitate communication between humans and computers. 
% While these include devices requiring movement such as touch and voice enabled keyboards, assistive human computer interfaces can be made accessible to those with LIS and related disorders if they act on signals produced using their available forms of movement: eye movement \citep{Bauer1979}. In our project, we create and test multiple assistive HCIs, specifically virtual keyboards (VK) that are capable of converting interpreting patterns of blinks for text input.

Conventional keyboards act on physical key presses to form an interface between humans and computers. Thus, people with LIS and people experiencing similar levels of paralysis cannot use them. Assistive virtual keyboards can be made accessible to these people by designing them to act on signals produced by their available forms of movement: vertical eye movement \citep{Bauer1979}. The keyboards proposed in our experiment specifically interpret patterns of voluntary blinks that translate into typed text.

\subsection{Electrooculography in VKs}
Electrooculograms (EOG) are bio-signals induced by the movement of the eyes \citep{McGill}.
A resting voltage exists between the positively-charged front and negatively-charged back of the eye \citep{McGill}.
Blinking reliably produces well-defined voltage changes which can be observed and recorded on the scalp using electrodes \citep{McGill}. See Figure \ref{fig:bell} for an diagram of the eye movement during blinking and the correponding voltage changes.
These signals can be processed and analyzed to control VKs.

\begin{figure}[ht]
    \centering
    \includegraphics{bell.jpg}
    \caption{Characteristic voltage changes as a result of blinking. \citep{Berry2012}.}
    \label{fig:bell}
\end{figure}

\subsection {Virtual Keyboards}
Few blink-based VKs have been created for people with the level of paralysis seen with LIS.
One VK detects horizontal eye movement to cycle through elements on a seven-by-six grid of letters (e.g. Row 1: `A',`B',`C',`D',`E',`F') 
and uses blinks to select the highlighted letter to achieve a speed of 5 characters per minute to successfully type a target sentence \citep{Teja2015}. This speed serves as our primary benchmark in evaluating the efficacy of our own VKs as it is both similar in design to our VK and the highest speed achieved by an blink-based EOG-enabled VK utilizing a grid layout. 

Language models use statistics to assign probabilities to sequences of words and have applications in text prediction. 
The researchers who developed the aforementioned VK have recommended them to increase typing speeds (\citeyear{Teja2015}).
One method of language modeling is the character n-gram \citep{Roark2010}.
N-grams estimate the likelihood that, given a constant $n$, some letter follows $n-1$ previously typed characters.
One study examining the performance of language modeling in VKs found that Witten-Bell smoothed 8-grams (an algorithm that estimates the likelihood of a letter given the previous 7) to be the highest performing model they tested (\citeyear{Roark2010}). 
  
Recurrent neural networks (RNNs) are a class of neural networks that can recognize patterns in sequences of inputs to make predictions about real-world data.
While n-gram models can consider an arbitrary amount of previous letters in their predictions (experiments utilizing character-level models typically use 8), higher-order models are known to require much more memory and experience diminishing returns in performance above $n=5$ in word n-grams \citep{Ha2006}. Additionally, higher order n-gram models also suffer from data sparsity problems where huge amounts of data are required to generalize results for high values of $n$. By contrast, the memory requirements of RNNs are independent of the amount of data they process and instead are determined by the network structure. Additionally, previous studies have found that RNNs perform better than 4 and 5 word n-grams, suggesting that they may have similar utility in character prediction \citep{Ravuri2016}. They also do not suffer from the same data sparsity problems as n-grams do.

The T9 typing system is a predictive text technology that uses a phone keypad to allow users to type sentences using words from a pre-defined dictionary \citep{Longe2009}. Each number on a T9 keypad is associated with several letters and words are typed by pressing the keys corresponding to the letters the user wants to type (\citeyear{Longe2009}). Since each number on a T9 keypad is associated with several letters, sequences of numbers can correspond to multiple words. For example, the number sequence ``2665'' corresponds to both the words ``book'' and ``cool''. \cite{Longe2009} describes specifically the operation of T9 keyboards.
This system has been demonstrated to be faster and more accurate compared to the commonly used 6-by-6 grid of letters in a VK utilizing a different biosignal, electroencephalograms, through both simulated and real-world tests in reaching a target word \citep{Akram2013}. It therefore may be faster when applied to EOG VKs.

While these systems have been shown promise in their respective applications, we found no literature comparing their effectiveness with virtual keyboards where the user inputs text with a sequence of blinks and pauses.
Thus, we decided to compare four virtual keyboards.
The first three let users select from a group of letters, presented one at a time by order of likelihood of that letter coming next.
The first is a character-level 8-gram.
The second and third are RNNs with the difference being that the third takes into account the 8-gram probabilities in its own predictions.
The fourth keyboard uses the T9 system with a modified layout, making it T9-like (See Figure \ref{fig:T9}).
The performance of the keyboards will be evaluated with both simulated and practical performance metrics.

\begin{figure}[h]
    \centering
    \includegraphics{T9.png}
    \caption{Above is a representation of our T9 interface. The top textbox with the text ``hello, world.'' contains the sentence the user is meant to type when testing. The textbox under it shows the text typed thus far. The one-lined textbox at the bottom of the image shows the number-representation of the current word as it is typed to the left of the colon and all the words that match that pattern to its right.}
    \label{fig:T9}
\end{figure}