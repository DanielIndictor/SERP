t = tcpip('127.0.0.1', 13854, 'NetworkRole', 'client');
t.Terminator = 'CR';

helloBuffer = '{"enableRawOutput": true, "format": "Json"}' ;
authorizationData = '{"appName": "FooBar", "appKey": "cdcaf0d80610ffdc5c128bfae675355445e86e70", "enableRawOutput": true, "format": "Json"}';

fopen(t)
disp ('Connection established.')

fwrite(t, authorizationData);
disp('Initial data sent')

pause(1)

rawData = zeros(2500, 1);
tic
for i=1:2500
    data = jsondecode(fgetl(t));

    if isfield(data, 'rawEeg')
        rawData(i) = data.('rawEeg');
    else
        i = i - 1;
        continue
    end

%     if i > 300 & ~mod(i,10)
%         disp(i)
%         figure(1)
%         plot(rawData(i-250:i))
%     end

end
toc

fclose(t)

