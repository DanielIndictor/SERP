#ifndef NGRAM_HPP
#define NGRAM_HPP

#include <map>
#include <iostream>
#include <ostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <thread> // std::thread::hardware_concurrency(), nothing else.
#include <assert.h>

#include <boost/asio/thread_pool.hpp>
#include <boost/asio/post.hpp>
#include <json.hpp>

class NGram
{
    struct Node
    {
    public:
        typedef long long unsigned count_type ;
        std::map<char, Node> map;
        count_type count = 0;
    };

    template <typename Candidate,
          typename = typename std::enable_if<
              std::is_same< typename std::iterator_traits<Candidate>
                    ::value_type,
                    char >::value >::type >
    struct ForwardItValidator
    {
        typedef int type;
    };

    Node root_;

    unsigned n_;

    template <typename ForwardIt,
              typename = typename ForwardItValidator<ForwardIt>::type >
    void add(const ForwardIt &begin, const ForwardIt &end);
public:
    typedef Node::count_type count_type;

    NGram(const unsigned &n);
    NGram(const std::string &path);
    void load_recursive(const nlohmann::json &j, Node &node);

    unsigned n() const;

    void add_phrase(const std::string &prefix);

    template <typename ForwardIt,
              typename = typename ForwardItValidator<ForwardIt>::type >
    count_type count(const ForwardIt &begin, const ForwardIt &end) const;
    count_type count(const std::string &prefix) const;
    count_type count(const char &letter) const;

    template <typename ForwardIt,
              typename = typename ForwardItValidator<ForwardIt>::type >
    double probability(const char &character,
                       const ForwardIt &historyBegin,
                       const ForwardIt &historyEnd) const;

    // This one takes iterators from a range where
    // (history_with_char_end-1) points to the character itself.
    template <typename ForwardIt,
              typename = typename ForwardItValidator<ForwardIt>::type >
    double probability(const ForwardIt &history_with_char_begin,
                       const ForwardIt &history_with_char_end) const;

    template <typename ForwardIt,
              typename = typename ForwardItValidator<ForwardIt>::type >
    std::string predict_next_letter(const ForwardIt &history_begin,
                                    const ForwardIt &history_end,
                                    const std::string &classes) const;
    std::string predict_next_letter(const std::string &history,
                                    const std::string &classes) const;
    std::vector<std::string>
    predict_sentence(const std::string &sentence,
                     const std::string &classes) const;

    std::vector<std::vector<double> >
    predict_distributions_sentence(const std::string &sentence,
                                   const std::string &classes) const;

    void save(const std::string &path) const;
private:
    void save_recursive(nlohmann::json *j, const Node &node) const;
};


template <typename ForwardIt, class>
void NGram::add(const ForwardIt &begin, const ForwardIt &end)
{
    // Cut down string to size n_.
    auto cutEnd = begin + std::min((typename ForwardIt::difference_type)(n_),
                    end-begin);
    Node *current_node = &root_;

    for (auto it = begin; it != cutEnd; ++it)
    {
        current_node->count++;
        current_node = &(current_node->map[*it]);
    }
    current_node->count++;
}

template <typename ForwardIt, class>
NGram::count_type
NGram::count(const ForwardIt &begin, const ForwardIt &end) const
{

    // Cut down string to size n_.
    auto cutEnd = begin + std::min((typename ForwardIt::difference_type)(n_),
                   end-begin);

    Node const *current_node = &root_;

    for (auto it = begin; it != cutEnd; ++it)
    {
        auto mapIt = current_node->map.find(*it);
        if (mapIt == current_node->map.end())
        {
            return 0;
        }
        current_node = &mapIt->second;
    }
    return current_node->count;
}

template <typename ForwardIt, class>
double NGram::probability(const char &character,
                   const ForwardIt &history_begin,
                   const ForwardIt &history_end) const
{
    std::string history_with_character(1 + (history_end-history_begin),
                                       static_cast<char>(0));
    std::copy(history_begin,
              history_end,
              history_with_character.begin());
    history_with_character.back() = character;
    return probability(history_with_character.begin(),
                       history_with_character.end());

}

template <typename ForwardIt, class>
double NGram::probability(
                   const ForwardIt &history_with_char_begin,
                   const ForwardIt &history_with_char_end) const
{
    // Cut down history.
    auto real_history_begin = history_with_char_end
        - std::min(static_cast<typename ForwardIt::difference_type>(n_ - 1),
                       history_with_char_end - history_with_char_begin);

    if (1 == (history_with_char_end - real_history_begin))
    {
       return ((double) count(*history_with_char_begin)) / count("");
    }

    auto c_history = count(real_history_begin,
                           history_with_char_end - 1);

    auto c_history_with_character = count(real_history_begin,
                history_with_char_end);

    // This pointer will reference the node on the trie corresponding to the
    // last character in the history.
    Node const *node_ptr = &root_;
    for (auto it = real_history_begin;
         it != (history_with_char_end-1);
         ++it)
    {
        auto map_it = node_ptr->map.find(*it);
        if (map_it == node_ptr->map.end())
        {
            // If the character does not show up in the history
            // then we return the probability of the history backed-off
            // by one.
            return probability(history_with_char_begin + 1,
                               history_with_char_end);
        }
        node_ptr = &map_it->second;
    }
    Node::count_type n_1orgreater = std::count_if(node_ptr->map.begin(),
                                                  node_ptr->map.end(),
                                                  [](const auto &element){
        return element.second.count >= 1;
    });

//    std::cout << history_with_char_end - (history_with_char_begin + 1)
//              << ' ' << std::flush;

    // If the character does not show up in the history
    // then we return the probability of the history backed-off
    // by one.
    if (n_1orgreater == 0)
    {
        return probability(history_with_char_begin + 1, history_with_char_end);
    }

    auto lambda =
            1 - static_cast<double>(n_1orgreater) / (n_1orgreater + c_history);

    return lambda * c_history_with_character / c_history
            + (1 - lambda) * probability(history_with_char_begin + 1,
                                         history_with_char_end);
}

template <typename ForwardIt, class>
std::string NGram::predict_next_letter(const ForwardIt &history_begin,
                                const ForwardIt &history_end,
                                const std::string &classes) const
{
    // Cut down history.
    auto real_history_begin = history_end
        - std::min(static_cast<typename ForwardIt::difference_type>(n_ - 1),
                       history_end - history_begin);

    std::vector<std::pair<char, double>> characters_and_likelihoods
            (classes.size());

    std::transform(classes.begin(), classes.end(),
                   characters_and_likelihoods.begin(),
                   [&](const auto &ch)
    {
        return std::make_pair(ch,
                              probability(ch, real_history_begin, history_end));
    });
    std::sort(characters_and_likelihoods.begin(),
              characters_and_likelihoods.end(),
              [](const auto &a, const auto &b)
                {return a.second > b.second;});

    std::string letters_sorted_by_likelihood(classes.size(), '\0');

    std::transform(characters_and_likelihoods.begin(),
                   characters_and_likelihoods.end(),
                   letters_sorted_by_likelihood.begin(),
                   [](const auto &pair){return pair.first;});

    return letters_sorted_by_likelihood;
}

#endif // NGRAM_HPP
