#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <climits>
#include <iterator>
#include <thread>
#include <chrono>

#include "ngram.hpp"

const std::string classes = " ,.abcdefghijklmnopqrstuvwxyz";

void load(const std::string &path) {
    NGram asdf(path);
    std::cout << "Built!" << std::endl;
    std::string sentence = "this was a triumph";

    auto o = asdf.predict_sentence("",
                                                 classes);
    std::cout << o.size() << "asdf" << '\n';
    for (const auto &dist : o)
    {
        for (const double &prob : dist)
        {
            std::cout << prob << ' ';
        }
        std::cout << std::endl;
    }

    std::cout << ' ' << std::endl;

    for (auto it = sentence.begin(); it != sentence.end(); ++it)
    {
        for (auto ch : classes)
        {
            std::cout << asdf.probability(ch,
                                          sentence.begin(),
                                          it) << ' ';
        }
        std::cout << std::endl;
    }
}

void build()
{
    std::cout << std::dec << std::boolalpha;

    NGram ngram(8);

    std::vector<char> vec;

    unsigned long long sentence_size = 20000ull;
    //Will be used to obtain a seed for the random number engine
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, classes.size() - 1);
    std::string gen_ngram(sentence_size, 'Z');

    std::generate_n(gen_ngram.begin(),
                    gen_ngram.size(),
                    [&](){return classes.at(dis(gen));});
    ngram.add_phrase(gen_ngram);

//    std::string predictme(100, 'Z');
//    std::generate_n(predictme.begin(),
//                    predictme.size(),
//                    [&](){return classes.at(dis(gen));});

//    auto a = ngram.predict_sentence(predictme, classes);
    ngram.save("/tmp/out.json");
}

int main()
{

    load("/home/daniel/Documents/Work/SERP/wittenbell_brown_v2.json");

    return 0;
}
