 #include "ngram.hpp"

NGram::NGram(const unsigned &n) : n_(n)
{

}

unsigned NGram::n() const
{
    return this->n_;
}

void NGram::add_phrase(const std::string &prefix)
{
    for (auto it = prefix.begin(); it != prefix.end(); ++it)
    {
        add(it, prefix.end());
    }
}

NGram::count_type NGram::count(const std::string &prefix) const
{
    return count(prefix.begin(), prefix.end());
}

NGram::count_type NGram::count(const char &prefix) const
{
    auto map_it = root_.map.find(prefix);
    if (map_it == root_.map.end())
    {
        return 0;
    }
    return map_it->second.count;
}

std::string NGram::predict_next_letter(const std::string &history,
                                       const std::string &classes) const
{
    return predict_next_letter(history.begin(), history.end(), classes);
}

std::vector<std::string>
NGram::predict_sentence(const std::string &sentence,
                         const std::string &classes) const
{
    boost::asio::thread_pool pool(std::thread::hardware_concurrency());

    std::vector<std::string> output(sentence.size() + 1);
    auto output_it = output.begin();
    for (auto it=sentence.begin(); it <= sentence.end(); ++it)
    {
        boost::asio::post(pool, [&, it, output_it] ()
            {
                *output_it = predict_next_letter(sentence.begin(), it, classes);
            });

        ++output_it;
    }
    pool.join();
    return output;

}

std::vector<std::vector<double>>
NGram::predict_distributions_sentence(const std::string &sentence,
                                      const std::string &classes) const
{
    boost::asio::thread_pool pool(std::thread::hardware_concurrency());

    std::vector<std::vector<double>> output(
            sentence.size() + 1, // Also predict for empty history.
            std::vector<double>(classes.size()));

    for (std::vector<std::vector<double>>::size_type class_indx = 0 ;
         class_indx != classes.size();
         ++class_indx)
    {
        boost::asio::post(pool, [&, class_indx] ()
        {
            for (std::vector<double>::size_type sentence_indx = 0;
                 sentence_indx <= sentence.size();
                 ++sentence_indx)
            {
                output[sentence_indx][class_indx] =
                        probability(classes[class_indx],
                                    sentence.begin(),
                                    sentence.begin() + sentence_indx);
            }
        });

    }
    pool.join();
    return output;

}

void NGram::save_recursive(nlohmann::json *j, const Node &node) const
{
    j->operator[]("count") = node.count;
    for (auto map_it = node.map.begin(); map_it != node.map.end(); ++map_it)
    {
        save_recursive(&j->operator[]("children")
                           [std::string(1, map_it->first)],
                           map_it->second);
    }
}
void NGram::save(const std::string &path) const
{
    using json = nlohmann::json;
    json j;
    j["n"] = n_;
    save_recursive(&j, root_);
    std::ofstream(path) << std::setw(2) << j;
}

NGram::NGram(const std::string &path)
{
    nlohmann::json j;
    std::ifstream(path) >> j;
    n_ = j.at("n").get<unsigned>();
    load_recursive(j, root_);

}

void NGram::load_recursive(const nlohmann::json &j, Node &node)
{
    node.count = j.at("count").get<count_type>();
    auto children = j.find("children");
    if (children == j.end())
    {
        return;
    }

    for(auto &element : children->get<nlohmann::json::object_t>())
    {
        load_recursive(element.second, node.map[element.first.front()]);
    }
}
