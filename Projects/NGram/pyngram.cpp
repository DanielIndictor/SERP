#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include "ngram.hpp"

class NGramAdapter : public NGram
{
public:
    using NGram::NGram;
    inline double probability_adapter(const char &character,
                                      const std::string &history)
    {
        return this->probability(character, history.begin(), history.end());
    }
};

BOOST_PYTHON_MODULE(pyngram)
{
    using namespace boost::python;
    class_<std::vector<std::string> >("StringVec")
        .def(vector_indexing_suite<std::vector<std::string> >())
    ;
    class_<std::vector<std::vector<double> > >("DistributionVec")
        .def(vector_indexing_suite<std::vector<std::vector<double> > >())
    ;
    class_<std::vector<double> >("Distribution")
        .def(vector_indexing_suite<std::vector<double> >())
    ;
    class_<NGramAdapter>("NGram", init<unsigned>())
            .def(init<std::string>()) // Construct from file at path.
            .add_property("n", &NGramAdapter::n)
            .def("probability", &NGramAdapter::probability_adapter)
            .def<void (NGram::*)(const std::string&)>
                ("add_phrase", &NGramAdapter::add_phrase)
            .def<std::string (NGramAdapter::*)(const std::string&,
                                        const std::string&) const>
                ("predict_next_letter", &NGramAdapter::predict_next_letter)
            .def("predict_sentence", &NGramAdapter::predict_sentence)
            .def("predict_distribution_sentence",
                 &NGramAdapter::predict_distributions_sentence)
            .def<NGramAdapter::count_type
                    (NGramAdapter::*)(const std::string&) const>
                ("count", &NGramAdapter::count)
            .def("save", &NGramAdapter::save)
            ;
}

