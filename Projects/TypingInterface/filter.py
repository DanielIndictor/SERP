from scipy import signal
import numpy as np
import matplotlib.pyplot as plt


def butter_bandpass(lowcut, highcut, fs_, order=5):
    nyq = 0.5 * fs_
    low = lowcut / nyq
    high = highcut / nyq
    b, a = signal.butter(order, [low, high], btype='band')
    return b, a


def butter_lowpass(cut, fs, order=18):
    nyq = 0.5 * fs
    normal_cut = cut / nyq
    b, a = signal.butter(order, normal_cut, btype='lowpass')
    return b, a


def butter_highpass(cut, fs, order=5):
    nyq = 0.5 * fs
    normal_cut = cut / nyq
    b, a = signal.butter(order, normal_cut, btype='highpass')
    return b, a


data = np.load('/tmp/asdf.npy')[100:]

fs = 250  # Sample frequency (Hz)
f0 = 60.0  # Frequency to be removed from signal (Hz)
Q = 60.0  # Quality factor
w0 = f0/(fs/2)  # Normalized Frequency
# Design notch filter
# b, a = butter_bandpass(.1, 55, 250, order=1)
b, a = butter_lowpass(57, 250, order=18)
# b, a = signal.iirnotch(w0, Q)
z = signal.lfilter_zi(b, 1)
result = np.zeros(data.size)

for i, x in enumerate(data):
    result[i], z = signal.lfilter(b, 1, [x], zi=z)

plt.plot(np.arange(data.size), data, np.arange(data.size), result)
plt.show()

print(np.sort(np.diff(data[100:])))