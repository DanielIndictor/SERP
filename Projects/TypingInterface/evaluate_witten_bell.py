import time

from typinginterface.clean_corpus import load_sentences
from typinginterface.wittenbell_model import WittenBellModel


def total_bits_witten_bell(sentences):
    corpus_path = '/home/daniel/Documents/Work/SERP/brown_training.txt'
    with open(corpus_path) as f:
        corpus = f.read()

    m = WittenBellModel(
        path='/home/daniel/Documents/Work/SERP/wittenbell_brown_v2.json')

    bits = 0
    time_begun = time.time()
    num_sentences = len(sentences)
    for sentence_num, sentence in enumerate(sentences):
        if len(sentence) == 0:
            continue

        # Add beginning of sentence marker ('.').
        # Guess indexed from 1 onwards because predict_sentence
        # starts with a prediction for an empty history
        # while the history actually starts with a period marking the beginning
        # of the sentence.
        guesses = m.predict_sentence('.' + sentence[:-1])
        # print(sentence, len(sentence), len(guesses))
        for guess, actual_ch in zip(guesses, sentence):
            # print(actual_ch, guess)
            bits += guess.index(actual_ch) + 1
        if sentence_num % 100 == 0:
            print(('{} sentences tested in ' +
                   '{} seconds of ' +
                   '{} sentences total.').format(sentence_num,
                                                 time.time() - time_begun,
                                                 num_sentences))
    return bits


if __name__ == '__main__':
    sentences = \
        load_sentences('/home/daniel/Documents/Work/SERP/brown_test.txt')

    total_characters = sum([len(sentence) for sentence in sentences])
    wb_bits_per_character = total_bits_witten_bell(sentences) \
                            / total_characters

    print('Witten Bell Bits Per Character: ' + str(wb_bits_per_character))
    with open('/home/daniel/Documents/Work/SERP/wbbpc.txt', 'w') as f:
        f.write('Witten Bell Bits Per Character: ' +
                str(wb_bits_per_character))
