import time

from typinginterface.clean_corpus import load_sentences
from typinginterface.compound_rnn_model import CompoundRNNLanguageModel
from typinginterface.wittenbell_model import WittenBellModel


def total_bits_rnn(sentences):
    wb = WittenBellModel(path='/home/daniel/Documents/Work/SERP'
                              '/wittenbell_brown_v2.json')
    m = CompoundRNNLanguageModel(
        model_path='/home/daniel/Documents/Work/SERP/'
                   'compound_lstm_brown_epoch_10.h5py',
        witten_bell_model=wb)

    bits = 0
    time_begun = time.time()
    num_sentences = len(sentences)
    for sentence_num, sentence in enumerate(sentences):

        guesses = m.predict_whole_sentence(sentence[:-1])
        for character, guess_ranking in zip(sentence, guesses):
            bits += 1 + guess_ranking.index(character)
        if sentence_num % 100 == 99:
            print(('{} sentences tested in ' +
                   '{} seconds of ' +
                   '{} sentences total.').format(sentence_num + 1,
                                                 time.time() - time_begun,
                                                 num_sentences))
    return bits


if __name__ == '__main__':
    sentences = load_sentences(
        '/home/daniel/Documents/Work/SERP/brown_test.txt')

    total_characters = sum([len(sentence) for sentence in sentences])
    total_sentences = len(sentences)

    rnn_bits_per_character = total_bits_rnn(sentences) / total_characters
    print('Compound-RNN Bits Per Character: ' + str(rnn_bits_per_character))
    with open('/home/daniel/compoundrnnbpc.txt', 'w') as f:
        f.write('Compound-RNN Bits Per Character: ' +
                str(rnn_bits_per_character))
