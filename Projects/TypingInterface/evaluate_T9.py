import re

from typinginterface.T9 import T9
from typinginterface.clean_corpus import load_sentences

if __name__ == '__main__':
    data = ' '.join(  # Space is intentional.
        load_sentences('/home/daniel/Documents/Work/SERP/brown_test.txt'))
    data += ' '

    t9 = T9('/usr/share/dict/american-english')

    letter_costs = {' ': 0, ',': 3, '.': 4,
                    '1': 3, '2': 4, '3': 5,
                    '4': 4, '5': 5, '6': 6,
                    '7': 5, '8': 6, '9': 7}

    # Not all words are in the T9 dictionary, so we must manually add up only
    # the characters of the words we analyzed.
    characters_analyzed = 0
    bits = 0

    # Analyze words.
    token_matcher = re.compile(r'\w+|[^\w]+')
    num_unmatched_words = 0
    unmatched_words = set()

    for token in token_matcher.findall(data):
        if token.isalpha():
            options = \
                list(set([word.lower().replace('\'', '') for word
                          in t9.possible_words(t9.to_num(token))]))
            if token in options:
                # If it's an option then add the bit cost of the word.
                for ch in token:
                    bits += letter_costs[t9.to_num(ch)]
                # Add one to position for selection
                # as well as the cost of selecting 'Make Prediction'
                bits += options.index(token) + 1 + letter_costs['1']
                characters_analyzed += len(token)
            else:
                num_unmatched_words += 1
                unmatched_words.add(token)

        else:
            # Since spaces are added automatically backspace must be pressed
            # to remove spaces where they don't belong such as in 'u.n.'
            if ' ' not in token and ('.' in token or ',' in token):
                bits += 2  # Two bits to select backspace.
            for ch in token:
                bits += letter_costs[ch]
            characters_analyzed += len(token)

    print('{} words remain unmatched total. '
          'There are {} unique unmatched words.'.format(num_unmatched_words,
                                                        len(unmatched_words)))
    for word in unmatched_words:
        print('\t{}'.format(word))

    print('T9 bits per character: {}'.format(bits / characters_analyzed))
    with open('/home/daniel/t9bpc.txt', 'w') as f:
        f.write('T9 bits per character: {}'.format(bits / characters_analyzed))
