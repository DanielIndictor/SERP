import time

from typinginterface.clean_corpus import load_sentences
from typinginterface.rnn_model import RNNLanguageModel


def total_bits_rnn(sentences):
    m = RNNLanguageModel('/home/daniel/Documents/Work/SERP/'
                         'lstm_brown_epoch_7.h5py')

    bits = 0
    time_begun = time.time()
    num_sentences = len(sentences)
    for sentence_num, sentence in enumerate(sentences):
        guesses = list(m.predict_whole_sentence(sentence[:-1]))
        for character, guess_ranking in zip(sentence, guesses):
            bits += 1 + guess_ranking.index(character)

        if sentence_num % 100 == 99:
            print(('{} sentences tested in ' +
                   '{} seconds of ' +
                   '{} sentences total.').format(sentence_num + 1,
                                                 time.time() - time_begun,
                                                 num_sentences))
    return bits


if __name__ == '__main__':
    sentences = load_sentences(
        '/home/daniel/Documents/Work/SERP/brown_test.txt')

    total_characters = sum([len(sentence) for sentence in sentences])
    total_sentences = len(sentences)

    rnn_bits_per_character = total_bits_rnn(sentences) / total_characters
    print('RNN Bits Per Character: ' + str(rnn_bits_per_character))
    with open('/home/daniel/rnnbpc.txt', 'w') as f:
        f.write('RNN Bits Per Character: ' + str(rnn_bits_per_character))
