import time

import openbci as ob
import matplotlib.pyplot as plt
import numpy as np

class sample_handler:
    def __init__(self, board):
        self.b = 0
        self.a = []
        self.board = board

    def handle_sample(self, sample):
        self.a.append(sample.channel_data[0])
        print(len(self.a), self.a[-4:-1])
        if self.b == 4000:
            np.save('/tmp/asdf.npy', self.a[1:])
            plt.plot(self.a[1:])
            plt.show()
            self.board.stop()
            exit(0)
        self.b += 1




board = ob.OpenBCICyton()
# board.disable_filters()
board.print_register_settings()
handler = sample_handler(board)
board.start_streaming(handler.handle_sample)

