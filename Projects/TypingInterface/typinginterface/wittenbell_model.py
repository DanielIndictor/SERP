from typinginterface.pyngram import NGram as _NGram

from typinginterface.clean_corpus import load_sentences
from typinginterface.language_model import LanguageModel


class WittenBellModel(LanguageModel):
    """Predicts the next character with n-grams."""

    def __init__(self, **kwargs):
        if 'path' in kwargs:
            self._ngram = _NGram(kwargs['path'])
        else:
            self._ngram = _NGram(kwargs['n'])

    def save(self, path):
        self._ngram.save(path)

    def train(self, sentences):
        for sentence in sentences:
            # Adding beginning-of-sentence marker '.'.
            self._ngram.add_phrase('.' + sentence)

    def count(self, string):
        return self._ngram.count(string)

    def _predict(self, history):
        return {character: self.probability(character, history)
                for character in self.classes}

    def probability(self, character, history):
        return self._ngram.probability(character, history)

    def predict_next_character(self, history):
        return self._ngram.predict_next_letter(history[-1 * self._ngram.n:],
                                               self.classes)

    def predict_sentence(self, sentence):
        """Makes predictions for each character in the sentence.
        C++ implementation is multithreaded, so use for benchmarking."""
        return self._ngram.predict_sentence(sentence, self.classes)[1:]

    def predict_distribution_sentence(self, sentence):
        """Makes predictions for each character in the sentence.
        C++ implementation is multithreaded, so use for benchmarking."""
        return self._ngram.predict_distribution_sentence(sentence,
                                                         self.classes)[1:]


if __name__ == '__main__':
    corpus_path = '/home/daniel/Documents/Work/SERP/brown_training.txt'

    m = WittenBellModel(n=8)
    m.train(load_sentences(corpus_path))
    m.save('/home/daniel/Documents/Work/SERP/wittenbell_brown_v2.json')
