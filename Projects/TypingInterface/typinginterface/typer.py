from PyQt5 import QtCore, QtGui, QtWidgets


def _font_of_size(size):
    """
    Convenience function to automatically generate QFonts of an intended size.
    :param size: Size of new font.
    :return: QFont of given point size.
    """
    ret = QtGui.QFont()
    ret.setPointSize(size)
    return ret


class Typer(QtWidgets.QMainWindow):
    TIMEOUT_REACTION = 2000
    TIMEOUT_NORMAL = 1500

    # TIMEOUT_REACTION = TIMEOUT_NORMAL = 1250

    def __init__(self, tracker, t9=None, language_models={}):
        super().__init__()

        self.tracker = tracker
        self.tracker.blinked.connect(self.typer_blinked)
        print('here')
        self.setStyleSheet('* {background-color: black; color: lightgray;}'
                           'QFrame#separator {border-style: outset; '
                           'border-width: 2px; '
                           'border-color: rgb(90, 90, 90);}'
                           'QFrame#t9_separator {border-style: outset; '
                           'border-width: 2px; '
                           'border-color: rgb(90,90,90);}')

        self._w = {}  # Stores widgets.
        self._w['central'] = QtWidgets.QWidget(self)
        self.setCentralWidget(self._w['central'])
        # self._w['central_l'] = QtWidgets.QVBoxLayout(self._w['central'])
        self._w['central'].setLayout(QtWidgets.QVBoxLayout())
        self._w['central_l'] = self._w['central'].layout()

        # Menubar.
        self._w['menu_bar'] = QtWidgets.QMenuBar(self)
        self.setMenuBar(self._w['menu_bar'])
        self._w['bci_controls'] = QtWidgets.QMenu(self._w['menu_bar'])
        self._w['bci_controls'].setTitle('BCI')
        self._w['menu_bar'].addAction(self._w['bci_controls'].menuAction())
        self._w['bci_start'] = QtWidgets.QAction(self._w['bci_controls'])
        self._w['bci_start'].setText('Start')
        self._w['bci_start'].triggered.connect(self.tracker.enable)
        self._w['bci_start'].setShortcut('Ctrl+W')
        self._w['bci_controls'].addAction(self._w['bci_start'])
        self._w['bci_stop'] = QtWidgets.QAction(self._w['bci_controls'])
        self._w['bci_stop'].setText('Stop')
        self._w['bci_stop'].triggered.connect(self.tracker.disable)
        self._w['bci_stop'].setShortcut('Ctrl+E')
        self._w['bci_controls'].addAction(self._w['bci_stop'])
        self._w['typer'] = QtWidgets.QMenu(self._w['menu_bar'])
        self._w['typer'].setTitle('Typer')
        self._w['menu_bar'].addAction(self._w['typer'].menuAction())
        self._w['typer_activate'] = QtWidgets.QAction(self._w['bci_controls'])
        self._w['typer_activate'].setText('Activate')
        self._w['typer'].addAction(self._w['typer_activate'])
        self._w['typer_activate'].setShortcut('Ctrl+Shift+A')
        self._w['typer_activate'].triggered.connect(self.typer_blinked)

        # Target textbox
        self._w['target'] = QtWidgets.QPlainTextEdit()
        self._w['target'].setFont(_font_of_size(20))
        self._w['target'].setSizePolicy(
            QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum,
                                  QtWidgets.QSizePolicy.Expanding))
        self._w['central_l'].addWidget(self._w['target'])
        self._w['tabs'] = QtWidgets.QTabWidget()
        self._w['central_l'].addWidget(self._w['tabs'])

        # T9
        self._w['tab_t9'] = T9Typer(t9)
        self._w['tabs'].addTab(T9Typer(t9), 'T9')

        # Language models.
        self._w['tab_models'] = []
        for name, model in language_models.items():
            tab_widget = LanguageModelTyper(model)
            self._w['tabs'].addTab(tab_widget, name)
            self._w['tab_models'].append(tab_widget)

        self._normal_timer = QtCore.QTimer()
        self._normal_timer.setInterval(self.TIMEOUT_REACTION)
        self._normal_timer.setSingleShot(True)
        self._normal_timer.timeout.connect(self.typer_timeout)
        self._normal_timer.start()

    def typer_timeout(self):
        self._normal_timer.stop()
        self._w['tabs'].currentWidget().activate()
        QtWidgets.QApplication.processEvents()
        self._normal_timer.start(self.TIMEOUT_REACTION)

    def typer_blinked(self):
        self._normal_timer.stop()
        self._w['tabs'].currentWidget().push()
        self._normal_timer.start(self.TIMEOUT_NORMAL)
        QtWidgets.QApplication.processEvents()
        print('BLINKED')


class T9Typer(QtWidgets.QWidget):
    _space = chr(0x25c7)
    _backspace = chr(0x232b)
    _num_rows = 4
    _num_columns = 3

    @property
    def text(self):
        return self.text_box.toPlainText().replace(self._space, ' ')

    @text.setter
    def text(self, text):
        self.text_box.setPlainText(text.replace(' ', self._space))

    @property
    def pos_xy(self):
        return self._pos_xy

    @pos_xy.setter
    def pos_xy(self, xy):
        self._pos_xy = xy
        self.paint_pos_xy()

    @property
    def typed_numbers(self):
        return self._typed_numbers

    @typed_numbers.setter
    def typed_numbers(self, typed_numbers):
        self._typed_numbers = typed_numbers
        self._options = \
            list(set([word.lower().replace('\'', '') for word
                      in self.t9.possible_words(self._typed_numbers)]))
        self.paint_selection_options()

    def paint_selection_options(self):
        if len(self._options) == 0:
            self.reset_position()
        possible_words = list(self._options)
        if self.selecting == 'SEL' and len(possible_words) > self.option_idx:
            possible_words[self.option_idx] = \
                '<font color=\"yellow\">{}</font>'.format(
                    possible_words[self.option_idx])
        if len(possible_words) <= self.option_idx:
            self.option_idx = 0

        self.selection_options.setText(
            '<font color=\"red\">{}</font>: {}'.format(
                self.typed_numbers,
                ' '.join(possible_words)))

    def paint_pos_xy(self):
        for button in self.t9_buttons.values():
            button.setStyleSheet('* {background-color: black}')

        if self.selecting == 'ROW':
            for i in range(self._num_columns):
                self.t9_buttons[i, self._pos_xy[1]] \
                    .setStyleSheet('* {background-color: blue}')
        elif self.selecting == 'COL':
            self.t9_buttons[self._pos_xy[0], self._pos_xy[1]] \
                .setStyleSheet('* {background-color: blue}')

    def __init__(self, t9, parent=None):
        super().__init__(parent=parent)

        self.t9 = t9
        QtWidgets.QVBoxLayout(self)

        self.text_box = QtWidgets.QPlainTextEdit()
        self.text_box.setFont(_font_of_size(20))
        self.text_box.setSizePolicy(
            QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum,
                                  QtWidgets.QSizePolicy.Expanding))
        self.layout().addWidget(self.text_box)
        self.grid = QtWidgets.QGridLayout()
        self.layout().addLayout(self.grid)

        # Next few blocks add all the buttons to the grid..
        self.t9_buttons = {}
        self.t9_button_font = _font_of_size(55)
        self.t9_label_font = _font_of_size(15)
        self.t9_labels = {}

        self.t9_buttons[0, 0] = QtWidgets.QPushButton(text=self._backspace)
        self.t9_buttons[1, 0] = QtWidgets.QPushButton(text=',')
        self.t9_buttons[2, 0] = QtWidgets.QPushButton(text='.')
        self.t9_buttons[0, 0].setFont(self.t9_button_font)
        self.t9_buttons[1, 0].setFont(self.t9_button_font)
        self.t9_buttons[2, 0].setFont(self.t9_button_font)
        self.grid.addWidget(self.t9_buttons[0, 0], 0, 0)
        self.grid.addWidget(self.t9_buttons[1, 0], 0, 1)
        self.grid.addWidget(self.t9_buttons[2, 0], 0, 2)

        self.t9_labels[0, 0] = QtWidgets.QLabel(font=self.t9_label_font)
        self.t9_labels[1, 0] = QtWidgets.QLabel(font=self.t9_label_font)
        self.t9_labels[2, 0] = QtWidgets.QLabel(font=self.t9_label_font)
        self.grid.addWidget(self.t9_labels[0, 0], 1, 0)
        self.grid.addWidget(self.t9_labels[1, 0], 1, 1)
        self.grid.addWidget(self.t9_labels[2, 0], 1, 2)

        for row in range(1, 4):
            for column in range(3):
                text = str((row - 1) * 3 + column + 1)
                t9_button = QtWidgets.QPushButton(text=text)
                # t9_button.setSizePolicy(self.grid_button_size_policy)
                t9_button.setFont(self.t9_button_font)
                # t9_button.clicked.connect(  # Currently buggy.
                #     lambda _: self.select_button(t9_button))
                self.grid.addWidget(t9_button, row * 2, column)
                self.t9_buttons[column, row] = t9_button

                label = QtWidgets.QLabel()
                label.setFont(self.t9_label_font)
                label.setAlignment(QtCore.Qt.AlignCenter)
                label.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                              QtWidgets.QSizePolicy.Fixed)
                self.grid.addWidget(label, row * 2 + 1, column)
                self.t9_labels[column, row] = label

        self.t9_labels[0, 1].setText('<i>Change Prediction</i>')
        self.t9_labels[1, 1].setText('ABC')
        self.t9_labels[2, 1].setText('DEF')
        self.t9_labels[0, 2].setText('GHI')
        self.t9_labels[1, 2].setText('JKL')
        self.t9_labels[2, 2].setText('MNO')
        self.t9_labels[0, 3].setText('PQRS')
        self.t9_labels[1, 3].setText('TUV')
        self.t9_labels[2, 3].setText('WXYZ')

        self.t9_line = QtWidgets.QFrame(self)
        self.t9_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.layout().addWidget(self.t9_line)
        self.selection_options = QtWidgets.QLabel(self)
        self.selection_options.setFont(_font_of_size(40))
        self.layout().addWidget(self.selection_options)

        # State management.
        self.option_idx = 0
        self.reset_state()

    def reset_position(self):
        self.selecting = 'ROW'  # Also available: 'COL', 'SEL'
        self.pos_xy = [0, 0]

    def reset_state(self):
        self.reset_position()
        self.text = ''
        self.typed_numbers = ''
        self.option_idx = 0  # Index of word highlighted.

    def push(self):
        if self.selecting == 'ROW':
            # Select next row.
            self.pos_xy = [0, (self.pos_xy[1] + 1) % self._num_rows]
        elif self.selecting == 'COL':
            # Select next column.
            self.pos_xy[0] = (self.pos_xy[0] + 1) % 3

        elif self.selecting == 'SEL':
            self.option_idx = (self.option_idx + 1) \
                              % len(self._options)
            self.paint_selection_options()
        self.paint_pos_xy()

    def activate(self):
        if self.selecting == 'ROW':
            self.pos_xy[0] = 0
            self.selecting = 'COL'
        elif self.selecting == 'COL':
            # Below function responsible for setting self.selecting
            self.select_button(self.t9_buttons[tuple(self.pos_xy)])
            self.pos_xy[1] = 0
        elif self.selecting == 'SEL':
            self.selecting = 'ROW'
            self.text = \
                self.text + \
                self._options[self.option_idx] + ' '
            self.typed_numbers = ''

        self.paint_pos_xy()
        QtWidgets.QApplication.processEvents()

    def select_button(self, button):
        button_text = button.text()
        if button_text == self._backspace:
            if len(self.typed_numbers) == 0 and len(self.text) > 0:
                self.text = self.text[:-1]
                # until a non-letter is found.
                # Get last typed word.
                # This may cut into an existing word, meaning that that word
                # is removed from text and its number representation
                # is restored.
                # Go backwards through the sentence
                if self.text.isalpha():
                    self.typed_numbers = self.t9.to_num(self.text)
                    self.text = ''
                else:
                    if len(self.text) > 0:
                        for i in range(len(self.text) - 1, -1, -1):
                            print(i)
                            print(self.text[i:].replace(' ', self._space))
                            if not self.text[i:].isalpha():
                                self.typed_numbers = \
                                    self.t9.to_num(self.text[i + 1:])
                                self.text = self.text[:i]
                                break
            else:
                self.typed_numbers = self.typed_numbers[:-1]
            self.paint_selection_options()
        elif button_text == ',':
            self.text += ', '
            self.typed_numbers = ''
        elif button_text == '.':
            self.text += '. '
            self.typed_numbers = ''
        elif button_text == '1':
            if len(self._options) == 0:
                self.typed_numbers = ''
                self.reset_position()
                self.paint_selection_options()
            else:
                self.selecting = 'SEL'
                self.paint_selection_options()

            self.option_idx = 0
            return
        elif button_text.isnumeric():
            self.typed_numbers += button_text
            print(button_text, self.typed_numbers)
            self.paint_selection_options()

        self.selecting = 'ROW'


class LanguageModelTyper(QtWidgets.QWidget):
    _space = chr(0x25c7)
    _backspace = chr(0x232b)

    @property
    def text_box_field(self):
        self._text = self.text_box.toPlainText()
        return self._text

    @text_box_field.setter
    def text_box_field(self, text):
        self._text = text
        self.update_text_box()

    def update_text_box(self):
        self.text_box.setPlainText(self._text)
        prediction = self.model.predict_next_character(
            '.' + self._text.replace(self._space, ' '))
        for button, ch in zip(self.buttons[1:], prediction):
            if ch == ' ':
                button.setText(self._space)
            else:
                button.setText(ch)

    @property
    def button_idx(self):
        return self._button_idx

    @button_idx.setter
    def button_idx(self, button_idx):
        self._button_idx = button_idx
        self.paint_button_idx()

    def paint_button_idx(self):
        for button in self.buttons:
            button.setStyleSheet('* {background-color: black}')

        self.buttons[self.button_idx].setStyleSheet(
            '* {background-color: blue}')

    def __init__(self, language_model, parent=None):
        super().__init__(parent=parent)

        self.model = language_model
        QtWidgets.QVBoxLayout(self)

        self.text_box = QtWidgets.QPlainTextEdit()
        # Weird recursion error.
        # self.text_box.textChanged.connect(self.update_text_box)
        self.text_box.setFont(_font_of_size(20))
        self.text_box.setSizePolicy(
            QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum,
                                  QtWidgets.QSizePolicy.Expanding))
        self.layout().addWidget(self.text_box)
        self.grid = QtWidgets.QGridLayout()
        self.layout().addLayout(self.grid)

        self.grid_button_font = _font_of_size(55)
        self.grid_button_subtext_font = _font_of_size(20)
        backspace_button = QtWidgets.QPushButton(text=self._backspace)
        backspace_button.setFont(self.grid_button_font)
        self.grid.addWidget(backspace_button, 0, 0)

        label = QtWidgets.QLabel(text='0')
        label.setAlignment(QtCore.Qt.AlignCenter)
        label.setFont(self.grid_button_subtext_font)
        self.grid.addWidget(label, 1, 0)
        self.buttons = [backspace_button]

        for idx in range(1, 30):
            button = QtWidgets.QPushButton()
            # t9_button.setSizePolicy(self.grid_button_size_policy)
            button.setFont(self.grid_button_font)
            self.grid.addWidget(button, (idx // 6) * 2, idx % 6)
            label = QtWidgets.QLabel(text=str(idx))
            label.setAlignment(QtCore.Qt.AlignCenter)
            label.setFont(self.grid_button_subtext_font)
            self.grid.addWidget(label, (idx // 6) * 2 + 1, idx % 6)
            self.buttons.append(button)


        # State management.
        self.reset_state()

    def reset_state(self):
        self.text_box_field = ''
        self.button_idx = 0  # Index of character highlighted.

    def push(self):
        self.button_idx = (self.button_idx + 1) % len(self.buttons)

    def activate(self):
        selected_ch = self.buttons[self.button_idx].text()
        if selected_ch == self._backspace:
            self.text_box_field = self.text_box_field[:-1]
        # elif selected_ch == self._space:  # Disabled for visibility
        #     self.text_box_field += ' '    # of double spaces.
        else:
            self.text_box_field += selected_ch
        self.button_idx = 0
        QtWidgets.QApplication.processEvents()
