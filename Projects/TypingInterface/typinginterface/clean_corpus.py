import re


# NOTE: These functions are specific to the version of the Brown Corpus
# noted in the paper due to the way it was formatted.
# Specifically, ampersands are used in place of periods where they don't denote
# the end of a sentence (ex. 'Mr.' or 'p.m.') and octothorps are used around
# article titles.

def _clean_corpus(text):
    # Change titles to sentences.
    # Change hyphens to spaces.
    # Throw out symbols other than letters, commas, ampersands and periods.
    # Next, remove whitespace before and after space.
    # Clean up whitespace.
    # Fix whitespace around commas and periods and collapse repetitions.
    substitutions = [[re.compile(r'^#(.*)#$', flags=re.MULTILINE), r'\1.'],
                     [re.compile(r'-+', flags=re.MULTILINE), ' '],
                     [re.compile(r'[^a-z.,!?&\s]', flags=re.MULTILINE), r''],
                     [re.compile(r'\s*[.!?]\s*', flags=re.MULTILINE), r'.'],
                     [re.compile(r'\s+', flags=re.MULTILINE), ' '],
                     [re.compile(r'\s*([,.])+', flags=re.MULTILINE), r'\1'],
                     ]

    cleaned_text = text.lower()
    for substitution in substitutions:
        cleaned_text = re.sub(*substitution, cleaned_text)

    return cleaned_text


def clean_corpus(text):
    return _clean_corpus(text).replace('&', '.')


def get_sentences(text):
    """ Cleans corpus as well.
    :param text: Input string from corpus.
    :return: Returns list of cleaned sentences.
    """
    return [(line.replace('&', '.') + '.')
            for line in _clean_corpus(text).split('.') if line != '']


def load_sentences(path):
    """Loads sentences delimited by newlines."""
    with open(path) as f:
        data = f.read()
    return [sentence for sentence in data.split('\n') if sentence != '']


def save_sentences(path, sentences):
    """Saves sentences delimited  by newlines."""
    with open(path, 'w') as f:
        f.write('\n'.join(sentences))


# Partition into training set (40,000) and training set (the rest of it).
def _main():
    file_path = '/home/daniel/Documents/Work/SERP/brown_corpus.txt'
    with open(file_path) as f:
        corpus = f.read()

    sentences = get_sentences(corpus)
    from random import shuffle
    shuffle(sentences)
    train = sentences[:40000]
    print(str(len(train)) + ' sentences in training set.')
    test = sentences[40000:]
    print(str(len(test)) + ' sentences in testing set.')
    save_sentences('/home/daniel/Documents/Work/SERP/brown_training.txt',
                   train)
    save_sentences('/home/daniel/Documents/Work/SERP/brown_test.txt',
                   test)


if __name__ == '__main__':
    _main()
