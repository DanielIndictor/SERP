import time

import keras


class ModelSaver(keras.callbacks.Callback):
    def __init__(self, n, batches_per_epoch, total_epochs, file_path_pattern):
        self.n = n
        self.batch = 0
        self.batches_per_epoch = batches_per_epoch
        self.begin_time = None
        self.total_epochs = total_epochs
        self.epoch = 0
        self.file_path_pattern = file_path_pattern

    def on_epoch_begin(self, epoch, logs=None):
        if self.begin_time is None:
            self.begin_time = time.time()

    def on_batch_end(self, batch, logs={}):
        self.batch += 1
        elapsed_time = time.time() - self.begin_time

        # total time in seconds is calculated by dividing elapsed time by
        # total number of batches that have already been run and then
        # multiplied by the total amount of batches to be run.
        # Next the elapsed time is subtracted from this total time
        # and the result is divided by 60**2 to convert to hours.
        predicted_time = \
            (elapsed_time /
             (self.batch + self.epoch * self.batches_per_epoch)
             * self.total_epochs * self.batches_per_epoch - elapsed_time) \
            / 60 / 60

        print('Epoch {}/{}, Batch {}/{}, {:.2f} seconds elapsed, Loss: {:.3f},'
              ' Predicted time left: {:2.2f}.'
              .format(self.epoch + 1,
                      self.total_epochs,
                      self.batch,
                      self.batches_per_epoch,
                      elapsed_time,
                      logs.get('loss'),
                      predicted_time
                      ))

    def on_epoch_end(self, epoch, logs={}):
        self.batch = 0
        self.epoch += 1
        self.model.save(self.file_path_pattern.format(self.epoch) + '.h5py')
        print('\n{} epochs trained in {} seconds of {} epochs total.'
              .format(self.epoch,
                      time.time() - self.begin_time,
                      self.total_epochs))
        with open(self.file_path_pattern.format('losses') + '.txt', 'a') as f:
            f.write('{} {}\n'.format(logs.get('loss'), logs.get('val_loss')))


class LossHistory(keras.callbacks.Callback):
    """Stores the loss for every batch"""

    def __init__(self):
        self.losses = []

    def on_epoch_end(self, epoch, logs={}):
        self.losses.append((logs.get('loss'), logs.get('val_loss')))

