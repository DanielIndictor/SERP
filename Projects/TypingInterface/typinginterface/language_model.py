import string

class LanguageModel:
    classes = ''.join(sorted(' ,.' + string.ascii_lowercase))

    def _predict(self, history):
        raise NotImplementedError()

    def predict(self, history):
        return self._predict(history)

    def predict_next_character(self, history):
        probabilities = self.predict(history)
        pairs = sorted([(probability, letter)
                        for letter, probability
                        in probabilities.items()],
                       reverse=True)
        return [letter for probability, letter in pairs]

    def predict_sentence(self, sentence):
        return [self.predict(fragment) for fragment in
                [sentence[:i] for i in range(len(sentence))]]
