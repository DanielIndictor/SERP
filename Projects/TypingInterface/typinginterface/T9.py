class T9:
    """Used to find the word corresponding to its keypad representation."""

    _translator = str.maketrans('abcdefghijklmnopqrstuvwxyz' +
                                'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                                '22233344455566677778889999' * 2)

    class _TreeNode:
        def __init__(self):
            self.words = []  # Words that can apply to the node
            # representing some numeric prefix.
            self.children = {}

    def __init__(self, dictionary_path: str):
        self.words = open(dictionary_path, 'r').read().split('\n')
        self._root_node = self._TreeNode()
        for word in self.words:
            self.add(word)

    def add(self, word):
        numeric = T9.to_num(word)

        current_node = self._root_node

        for number in numeric:
            if number not in current_node.children.keys():
                current_node.children[number] = self._TreeNode()
            current_node = current_node.children[number]
        current_node.words.append(word)

    @staticmethod
    def to_num(word):
        """Returns the keypad representation of some word as a string.

        Example: to_num('hello') -> '43556' """
        letters_only = ''.join(filter(lambda character: character.isalpha(),
                                      word))
        return letters_only.translate(T9._translator)

    def possible_words(self, numeric):
        """ Returns the possible words for a given keypad representation.

        Example: t9.possible_words('find') -> ['dime', 'dine', 'find', 'fine']
        """

        current_node = self._root_node
        for number in numeric:
            if number not in current_node.children.keys():
                return []
            current_node = current_node.children[number]
        return current_node.words


if __name__ == '__main__':
    t9 = T9('/usr/share/dict/american-english')
    print(t9.possible_words(T9.to_num('its')))
