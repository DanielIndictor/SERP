import random

import keras
import numpy as np

from typinginterface.callbacks import LossHistory, ModelSaver
from typinginterface.clean_corpus import load_sentences
from typinginterface.rnn_model import RNNLanguageModel
from typinginterface.wittenbell_model import WittenBellModel


# from keras.backend import set_session

# # config = tf.ConfigProto(inter_op_parallelism_threads=23, intra_op_parallelism_threads=23)
# config = tf.ConfigProto()
# # config.gpu_options.allow_growth = True
# config.gpu_options.per_process_gpu_memory_fraction = 0.8
# set_session(tf.Session(config=config))

class CompoundRNNLanguageModel(RNNLanguageModel):
    """Predicts the next character.
       To match training conditions, it should be reset at the end of each
       sentence."""

    def __init__(self, model_path=None, witten_bell_model=None):
        if model_path:
            self.model = keras.models.load_model(model_path)
        else:
            self.model = keras.Sequential([
                keras.layers.LSTM(1000,
                                  input_shape=(None, len(self.classes) * 2,),
                                  return_sequences=True),
                keras.layers.LSTM(100, return_sequences=True),

                keras.layers.TimeDistributed(
                    keras.layers.Dense(len(self.classes),
                                       activation=keras.activations.sigmoid))
            ])
            self.model.compile(optimizer=keras.optimizers.Adam(.001),
                               loss=keras.losses.categorical_crossentropy)

        try:
            pass  # self.model = keras.utils.multi_gpu_model(self.model)
        except:
            pass
        if witten_bell_model:
            self._witten_bell_model = witten_bell_model
        else:
            raise ValueError('Missing witten_bell_model!')

    def train(self, sentences, epoch_pattern, epochs=2):

        random.shuffle(sentences)
        validation_begin = (len(sentences) * 19) // 20
        validation_sentences = sentences[validation_begin:]
        training_sentences = sentences[:validation_begin]

        check = ModelSaver(500, len(training_sentences), epochs, epoch_pattern)

        loss_history = LossHistory()

        validation_generator = self.infinite_data_generator(
            self.data_generator, [validation_sentences])
        training_generator = self.infinite_data_generator(
            self.data_generator, [training_sentences])

        self.model.fit_generator(training_generator,
                                 epochs=epochs,
                                 use_multiprocessing=True,
                                 max_queue_size=1000,
                                 callbacks=[loss_history, check],
                                 steps_per_epoch=len(training_sentences),
                                 validation_data=validation_generator,
                                 validation_steps=len(validation_sentences),
                                 verbose=2
                                 )

        return loss_history.losses

        # Disabled use_multiprocessing in fit_generator
        # so that the data generator may spawn its own pool
        # of worker processes.

    def data_generator(self, sentences):
        sentences = list(sentences)
        random.shuffle(sentences)
        for sentence_num, sentence in enumerate(sentences):
            if len(sentence) == 0:
                return (np.zeros(shape=(0, 0, len(self.classes) * 2)),
                        np.zeros(shape=(0, 0, len(self.classes))))

            # Period below used to mark the start of the sentence so
            # that predictions cover the first character of a sentence.
            input_chars = '.' + sentence[:-1]

            last_letters = self._encode_string(input_chars)

            # Fill up Witten-Bell Predictions.
            witten_bell_predictions = \
                np.array(
                    self._witten_bell_model.predict_distribution_sentence(
                        input_chars))

            x = np.concatenate((last_letters, witten_bell_predictions), axis=1)
            # for i, row in enumerate(x):
            #     print(np.reshape(row, (2, 29)), file=sys.stdout)
            y = RNNLanguageModel._encode_string(sentence)

            yield np.expand_dims(x, axis=0), np.expand_dims(y, axis=0)

    def _predict(self, history):
        self.model.reset_states()
        # Period below used to mark the start of the sentence so
        # that predictions cover the first character of a sentence.
        input_chars = '.' + history

        last_letters = self._encode_string(input_chars)

        # Fill up Witten-Bell Predictions.
        witten_bell_predictions = \
            np.array(
                self._witten_bell_model.predict_distribution_sentence(
                    input_chars))


        x = np.concatenate((last_letters, witten_bell_predictions), axis=1)
        prediction = self.model.predict(np.expand_dims(x, axis=0),
                                         batch_size=len(history) + 2, )

        return dict(zip(self.classes, prediction[0, -1, :]))

    def predict_whole_sentence(self, history):
        if len(history) == 0:
            return (np.zeros(shape=(0, 0, len(self.classes) * 2)),
                    np.zeros(shape=(0, 0, len(self.classes))))

        # Period below used to mark the start of the sentence so
        # that predictions cover the first character of a sentence.
        input_chars = '.' + history

        last_letters = self._encode_string(input_chars)

        # Fill up Witten-Bell Predictions.
        witten_bell_predictions = \
            np.array(
                self._witten_bell_model.predict_distribution_sentence(
                    input_chars))

        x = np.concatenate((last_letters, witten_bell_predictions), axis=1)
        predictions = self.model.predict(np.expand_dims(x, axis=0),
                                         batch_size=len(history) + 2, )

        predictions = predictions.squeeze()
        for prediction_num, prediction in enumerate(predictions):
            probability_class_pairs = sorted(zip(prediction, self.classes),
                                             reverse=True)
            yield [character for probability, character
                   in probability_class_pairs]


if __name__ == '__main__':
    file_path = '/home/daniel/Documents/Work/SERP/brown_training.txt'

    sentences = load_sentences(file_path)

    total_characters = sum([len(sentence) for sentence in sentences])
    print('Training on {} characters total.'.format(total_characters))

    wb_path = ('/home/daniel/Documents/Work/SERP/wittenbell_brown_v2.json')
    wb = WittenBellModel(path=wb_path)
    m = CompoundRNNLanguageModel(
        model_path='/home/daniel/Documents/Work/SERP'
                   '/compound_lstm_brown_epoch_10.h5py',
        witten_bell_model=wb)

    # losses = m.train(sentences,
    #                  '/home/daniel/Documents/Work/SERP/' +
    #                  'compound_lstm_brown_epoch_7+{}.h5py',
    #                  epochs=10)

    losses = m.train(sentences,
                     '/home/daniel/Documents/Work/SERP/' +
                     'compound_lstm_brown_epoch_10+{}',
                     epochs=10)

    # file_name = '/home/daniel/Documents/Work/SERP/compound_losses.txt'
    # with open(file_name, 'a') as f:
    #     for loss in losses:
    #         f.write('{}, {}\n'.format(*loss))

    # Automatically saves.
    # m.save('/home/daniel/Documents/Work/SERP/compound_lstm_brown.h5py')
