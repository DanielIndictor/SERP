from multiprocessing import Event
from time import sleep

import numpy as np
from PyQt5 import QtCore

from .blinktracker import BlinkTracker


class OfflineTracker(BlinkTracker):

    def __init__(self, data, fs=250):

        super().__init__()

        self.data = data
        self.fs = fs
        self.active = Event()
        self.active.set()
        self.process = None

    def _start(self):
        smooth_data = np.convolve(self.data, np.ones(10) / 10, mode='valid')
        threshhold = -2e-5
        times_less_than_required = 0
        previous_value = smooth_data[0]

        for datum in smooth_data:
            if not self.active.is_set():
                return

            if datum - previous_value < threshhold:
                times_less_than_required += 1
            else:
                if times_less_than_required >= 7:
                    self.blinked.emit()
                times_less_than_required = 0
            previous_value = datum
            sleep(1 / self.fs)

    def start(self):
        # Start process
        if self.process is not None:
            raise RuntimeError('Already started.')

        self.process = QtCore.QThread()
        self.process.run = self._start
        self.process.start()


    def end(self):
        if self.process:
            self.active.clear()
            self.process.wait(10)
            del(self.process)
            self.process = None


    @QtCore.pyqtSlot()
    def await_end(self, timeout=None):
        if timeout:
            self.process.wait(timeout)
        else:
            self.process.wait()


    def __del__(self):
        if self.process:
            self.process.wait()