import time
from multiprocessing import Lock

import numpy as np
from PyQt5 import QtCore
from openbci import OpenBCICyton
from scipy import signal

from .blinktracker import BlinkTracker


def _butter_lowpass(cut, fs, order=18):
    nyq = 0.5 * fs
    normal_cut = cut / nyq
    b, a = signal.butter(order, normal_cut, btype='lowpass')
    return b, a


def _iirnotch(f0, fs, quality=60):
    fs = 250  # Sample frequency (Hz)
    Q = 60.0  # Quality factor
    w0 = f0 / (fs / 2)  # Normalized Frequency
    b, a = signal.iirnotch(w0, Q)
    return b, a


# Each row is a condition of the shape (samples, total_change, fudge_factor)
# where fudge_factor is the maximum amount the signal can deviate from
# total_change to still qualify.

# Daniel's config.
# _downswing_config = np.array([(6, -15, 50),
#                               (6, -200, 90),
#                               (6, -300, 100),
#                               (6, 0, 40)], dtype=np.int32)

_downswing_config = np.array([(6, 0, 80),
                              # (6, -200, 90),
                              (30, -750, 300)], dtype=np.int32)


# (3, 0, 80)], dtype=np.int32)


class CytonTracker(BlinkTracker):

    def __init__(self, fs=250, port=None):

        super().__init__()

        self._fs = fs
        self._enabled = [Lock(), False, ]
        self._process = None

        self._b, self._a = _iirnotch(60, self._fs)
        self._z = signal.lfilter_zi(self._b, 1)

        self._filtered_data = []
        self._board = OpenBCICyton(port=port)

        self._process = QtCore.QThread()
        self._process.run = self._start
        self._process.start()
        self._throwaway = True
        self._last_blink = 0

    def _start(self):
        self._board.start_streaming(self._handle_sample)

    def _handle_sample(self, sample):
        if self._throwaway:
            self._throwaway = False
            return

        datum = sample.channel_data[0]
        filtered_result, self._z = signal.lfilter(self._b,
                                                  1,
                                                  [datum],
                                                  zi=self._z)
        self._filtered_data.append(float(filtered_result))

        if (not self._enabled[1]) \
                or (len(self._filtered_data) < 1000) \
                or ((len(self._filtered_data) - self._last_blink) < 70):
            return

        is_downswing = True
        for condition_num, condition in enumerate(_downswing_config):
            begin = -1 * _downswing_config[:condition_num, 0].sum() - 1
            end = begin - condition[0]
            # if condition_num >= 1:
            #     print(begin, end,
            #           str(self._filtered_data[end] - self._filtered_data[
            #               begin]))

            qualifies = np.abs(
                ((self._filtered_data[end] - self._filtered_data[begin]) -
                 condition[1]) < condition[2])
            if not qualifies:
                is_downswing = False
                break

        # possible_upswing = np.sum(differences[-10:] > 20) > 7

        if is_downswing:
            self.blinked.emit()
            print('{} - Blinked!'.format(time.strftime('%H:%M:%S')))
            self._last_blink = len(self._filtered_data)

    def enable(self):
        with self._enabled[0]:
            self._enabled[1] = True

    def disable(self):
        with self._enabled[0]:
            self._enabled[1] = False
        np.savetxt('/tmp/asdf.csv', self._filtered_data, delimiter=',')

    def __del__(self):
        self._board.stop()

        if self._process:
            self._process.wait()
