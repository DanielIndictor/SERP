# Sample of how the Lab Streaming Layer may be used instead of a direct connection.
# Not being pursued due to latency concerns.

import numpy as np
from pylsl import StreamInlet, resolve_stream, local_clock
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

plot_duration = 2.0


# first resolve an EEG stream on the lab network
print("looking for an EEG stream...")
streams = resolve_stream('type', 'EEG')

# create a new inlet to read from the stream
inlet = StreamInlet(streams[0])

def update():
    # Read data from the inlet. Use a timeout of 0.0 so we don't block GUI interaction.
    chunk, timestamps = inlet.pull_chunk(timeout=0.0, max_samples=32)
    print(chunk)


timer = QtCore.QTimer()
timer.timeout.connect(update)
timer.start(50)


# Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()