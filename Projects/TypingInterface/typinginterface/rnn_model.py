import datetime
import random

import keras
import numpy as np
from sklearn.preprocessing import OneHotEncoder

from typinginterface.callbacks import LossHistory, ModelSaver
from typinginterface.clean_corpus import load_sentences
from typinginterface.language_model import LanguageModel


class RNNLanguageModel(LanguageModel):
    """Predicts the next character.
       To match training conditions, it should be reset at the end of each
       sentence."""

    encoder = OneHotEncoder(sparse=False)
    encoder.fit([[ord(character)] for character in LanguageModel.classes])

    @classmethod
    def _encode_string(cls, text):
        return cls.encoder.transform([[ord(character)] for character in text])

    def __init__(self, file_path=None):
        if file_path:
            self.load(file_path)
        else:
            self.model = keras.Sequential([
                keras.layers.LSTM(1000, input_shape=(None, len(self.classes),),
                                  return_sequences=True),
                keras.layers.LSTM(100, return_sequences=True),
                # keras.layers.LSTM(50, return_sequences=True),
                keras.layers.TimeDistributed(
                    keras.layers.Dense(len(self.classes),
                                       activation=keras.activations.sigmoid))
                ])
            self.model.compile(optimizer=keras.optimizers.Adam(.001),
                               loss=keras.losses.categorical_crossentropy)

    def load(self, file_path):
        self.model = keras.models.load_model(file_path)

    def save(self, file_path):
        return self.model.save(file_path, overwrite=True)

    @classmethod
    def data_generator(cls, sentences):
        for sentence in sentences:
            if len(sentence) == 0:
                input_chars = ''
                target_chars = ''
            else:
                # Period below used to mark the start of the sentence so
                # that predictions cover the first character of a sentence.
                input_chars = '.' + sentence[:-1]
                target_chars = sentence
            x = cls._encode_string(input_chars)
            y = cls._encode_string(target_chars)

            yield (np.expand_dims(x, axis=0), np.expand_dims(y, axis=0))

    @classmethod
    def infinite_data_generator(cls, generator_generator, args):
        """generator_generator is a callable that returns generators."""
        while True:
            gen = generator_generator(*args)
            for val in gen:
                yield val

    def train(self, sentences, epoch_pattern, epochs=1):
        random.shuffle(sentences)
        validation_begin = (len(sentences) * 19) // 20
        training_sentences = sentences[:validation_begin]
        validation_sentences = sentences[validation_begin:]
        print('Training on {} sentences and validating on {} sentences.'
              .format(len(training_sentences), len(validation_sentences)))
        check = ModelSaver(500, validation_begin, epochs, epoch_pattern)

        loss_history = LossHistory()

        validation_generator = self.infinite_data_generator(
            self.data_generator, [validation_sentences])
        training_generator = self.infinite_data_generator(
            self.data_generator, [training_sentences])

        self.model.fit_generator(training_generator,
                                 epochs=epochs,
                                 use_multiprocessing=False,
                                 workers=1,
                                 max_queue_size=10,
                                 verbose=0,
                                 callbacks=[loss_history, check],
                                 steps_per_epoch=len(training_sentences),
                                 validation_data=validation_generator,
                                 validation_steps=len(validation_sentences)
                                 )
        return loss_history.losses

    def _predict(self, history):
        self.model.reset_states()
        if history.startswith('.'):
            x = self._encode_string(history)
        else:
            x = self._encode_string('.' + history)

        prediction = self.model.predict(np.expand_dims(x, axis=0),
                                        batch_size=len(history)+2,)

        return dict(zip(self.classes, prediction[0, -1, :]))

    def predict_whole_sentence(self, history):
        """Returns predictions for all characters in history."""
        self.model.reset_states()
        if history.startswith('.'):
            x = self._encode_string(history)
        else:
            x = self._encode_string('.' + history)

        predictions = self.model.predict(np.expand_dims(x, axis=0),
                                        batch_size=len(history) + 2, )

        predictions = predictions.reshape(-1, len(self.classes))
        for prediction in predictions:
            probability_class_pairs = sorted(zip(prediction, self.classes),
                                             reverse=True)

            yield [character for probability, character
                   in probability_class_pairs]

if __name__ == '__main__':
    file_path = '/home/daniel/Documents/Work/SERP/brown_training.txt'

    sentences = \
        load_sentences(file_path)

    total_characters = sum([len(sentence) for sentence in sentences])

    print('Training on {} characters total.'.format(total_characters))

    m = RNNLanguageModel()
    losses = m.train(sentences,
                     '/home/daniel/Documents/Work/SERP/' +
                     'lstm_brown_epoch_{}.h5py',
                     epochs=7)
    file_name = '/home/daniel/Documents/Work/SERP/{} - lstm losses.txt' \
        .format(datetime.datetime.now().isoformat())

    with open(file_name, 'w') as f:
        f.write('loss, validation_loss\n')
        for loss in losses:
            f.write('{}, {}\n'.format(*loss))
    m.save('/home/daniel/Documents/Work/SERP/lstm_brown.h5py')
