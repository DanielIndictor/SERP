from PyQt5 import QtCore


class BlinkTracker(QtCore.QObject):
    blinked = QtCore.pyqtSignal()

    @QtCore.pyqtSlot()
    def enable(self):
        raise NotImplementedError()

    @QtCore.pyqtSlot()
    def disable(self):
        raise NotImplementedError()
