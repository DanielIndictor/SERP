import sys
import time

import scipy as sp
from PyQt5 import QtWidgets
from scipy import io

from typinginterface.T9 import T9
from typinginterface.cytontracker import CytonTracker
from typinginterface.typer import Typer
from typinginterface.wittenbell_model import WittenBellModel
from typinginterface.compound_rnn_model import CompoundRNNLanguageModel
from typinginterface.rnn_model import RNNLanguageModel

beginning = time.time()

tracker = CytonTracker()

t9 = T9('/usr/share/dict/american-english')
wb = WittenBellModel(path='/home/daniel/Documents/Work/SERP'
                          '/wittenbell_brown_v2.json')
# c_rnn = CompoundRNNLanguageModel(
#     model_path='/home/daniel/Documents/Work/SERP/'
#                'compound_lstm_brown_epoch_10.h5py',
#     witten_bell_model=wb)

rnn = RNNLanguageModel('/home/daniel/Documents/Work/SERP/'
;
# language_models = {'WittenBell': wb, 'Compound RNN': c_rnn, 'RNN': rnn}
language_models = {'RNN': rnn}

app = QtWidgets.QApplication(sys.argv)

application = Typer(tracker, t9=t9, language_models=language_models)
application.show()
sys.exit(app.exec_())