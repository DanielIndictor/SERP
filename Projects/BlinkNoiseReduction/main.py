import math
import sys
import numpy as np
import scipy as sp
import scipy.signal
import scipy.io
import obspy as op
import obspy.signal.filter

import matplotlib.pyplot as plt

FS = 250

# example.png generated with parameters '../../MATLAB/recordings/blinking.mat blinking'
if len(sys.argv) != 3 or not sys.argv[1].endswith('.mat'):
    print('You must specify exactly one MATLAB file path and the name of the variable that you want to analyze',
          file=sys.stderr)
    exit(-1)


data = sp.io.loadmat(sys.argv[1])[sys.argv[2]].flatten()
IIR_filter = op.signal.filter.highpass(data, .2, FS, corners=1)

convolution = np.convolve(data, np.ones(500) / 500, mode='valid')
movmean_filter = data[np.size(data) - np.size(convolution):] - np.convolve(data, np.ones(500) / 500, mode='valid')

x = np.linspace(0, len(data) / FS, num=len(data))

fig, axs = plt.subplots(1, 1, constrained_layout=True)

axs.set_title('Comparison of detrending methods')
axs.set_xlabel('Time (s)')
axs.set_ylabel('Voltage reading (V)')
axs.plot(x, IIR_filter * .5, label='High-pass filter')
axs.plot(x, sp.signal.detrend(data), label='Linear detrending')
axs.plot(x[np.size(data) - np.size(convolution):], sp.signal.detrend(movmean_filter), label='Moving average')
axs.legend()
# axs[1].set_title('Frequency Domain (PSD)')
# axs[1].psd(IIR_filter, Fs=FS, scale_by_freq=False)
# axs[1].psd(data, Fs=FS, scale_by_freq=False)

plt.show()