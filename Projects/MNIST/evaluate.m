load w1.mat
classes = 10;
% Binary files below not provided in repository to save space;
% you have to get them online.
images = loadMNISTImages('t10k-images-idx3-ubyte');
% Labels
labels = loadMNISTLabels('t10k-labels-idx1-ubyte');

m = length(labels);
biasedImages = [images; ones(1, m)];

% Number of correct guesses during training.
correct_training = 0;

% Hypothesis
h = w * biasedImages;

for i = 1:m

  % This is an ugly way to do it, so I'm going to vectorize the use of max later.

  % Get guess
  [mx, mx_index] = max(h(:, i));
  if (labels(i) + 1) == mx_index;
    correct_training = correct_training + 1;
  end
  disp(i)
end

disp(correct_training / i)
keyboard()
