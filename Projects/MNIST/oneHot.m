function hot = oneHot(numClasses, classLabels)
  hot = zeros(max(size(classLabels)), numClasses);
  for label = 1:max(size(classLabels))
    hot(label, classLabels(label) + 1) = 1;
  end
end
