inputSize = 28^2;
classes = 10;
learningRate = .1;

% Binary files below not provided in repository to save space;
% you have to get them online.
images = loadMNISTImages('train-images-idx3-ubyte');
% Labels
labels = loadMNISTLabels('train-labels-idx1-ubyte');

m = length(labels)

hotLabels = oneHot(classes, labels);
biasedImages = [images; ones(1, m)];

% Weights
w = zeros(classes, inputSize + 1);
% Biases
b = zeros(classes, 1);

epochs = 1;

for epochNum = 1:epochs

  % Correct Answers
  y = hotLabels';
  % Hypothesis
  h = w * biasedImages;
  w = w - (1/m) * learningRate * (h - y) * biasedImages';

  disp(epochNum)

end

save w1.mat w
