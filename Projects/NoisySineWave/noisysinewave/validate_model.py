import math

import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

wave_upper_bound = 2 * math.pi
input_size = 500
samples_per_wave_type = 10000

x_axis = np.linspace(0, wave_upper_bound, num=input_size)

x_sine = np.zeros([samples_per_wave_type, input_size])
x_sine += np.sin(x_axis)
y_sine = np.ones([samples_per_wave_type, 1])

x_empty = np.zeros([samples_per_wave_type, input_size])
y_empty = np.zeros([samples_per_wave_type, 1])

x_square = np.zeros([samples_per_wave_type, input_size])
x_square += signal.square(x_axis)
y_square = np.zeros([samples_per_wave_type, 1])

x_sawtooth = np.zeros([samples_per_wave_type, input_size])
x_sawtooth += signal.sawtooth(x_axis * 2)
y_sawtooth = np.zeros([samples_per_wave_type, 1])

x_triangle = np.zeros([samples_per_wave_type, input_size])
x_triangle += signal.sawtooth(x_axis + math.pi/2, .5)
y_triangle = np.zeros([samples_per_wave_type, 1])

# Add Noise
x_sine += np.random.uniform(-1, 1, size=np.shape(x_sine))
x_empty += np.random.uniform(-1, 1, size=np.shape(x_empty))
x_square += np.random.uniform(-1, 1, size=np.shape(x_square))
x_sawtooth += np.random.uniform(-1, 1, size=np.shape(x_sawtooth))
x_triangle += np.random.uniform(-1, 1, size=np.shape(x_triangle))
x = np.concatenate((x_sine, x_empty, x_square, x_sawtooth, x_triangle, ), axis=0)
y = np.concatenate((y_sine, y_empty, y_square, y_sawtooth, y_triangle, ), axis=0)


model = tf.keras.models.load_model('trained_model.hdf5', compile=True)

# Now to get accuracy.
predictions = np.round(model.predict(x))

accuracy = np.sum(y == predictions) / np.size(y)

print('Accuracy of trained network over all training data:', accuracy)

# f, axarr = plt.subplots(5, sharex=True, sharey=True)
# plt.title('Examples of noisy sine waves and emtpy noise.')
#
# axarr[0].plot(np.transpose(x_sine[:3]))
# axarr[0].set_title('Sine wave')
#
# axarr[1].plot(np.transpose(x_empty[:3]))
# axarr[1].set_title('Empty noise')
#
# axarr[2].plot(np.transpose(x_square[:3]))
# axarr[2].set_title('Square wave')
#
# axarr[3].plot(np.transpose(x_sawtooth[:3]))
# axarr[3].set_title('Sawtooth wave')
#
# axarr[4].plot(np.transpose(x_triangle[:3]))
# axarr[4].set_title('Triangle wave')
#
# plt.show()