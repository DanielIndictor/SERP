import tensorflow as tf


def get_model():

    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(50, activation=tf.keras.activations.softplus))
    model.add(tf.keras.layers.Dense(32, activation='relu'))
    model.add(tf.keras.layers.Dense(1, activation='sigmoid'))
    model.compile(optimizer=tf.keras.optimizers.Adam(0.001),
                  loss='binary_crossentropy',
                  metrics=['accuracy'])

    return model
