fclose(instrfindall)

clear x
i = 1;


steps = 400;            
% Samples per step.
stepSize = 20;
smoothingSamples = 10; % Amount of nearest values to use in moving average.
lookBack = 100; % Amount of steps to look back when calibrating minimum
                % difference necessary to count as an "edge".

% assert(steps > lookBack);              

x = zeros(stepSize * steps, 1);
% standardDev = 0; % Exponentially decaying standard deviation.
differences = zeros(size(x));

board = CytonBoard('COM4', (stepSize+5) * steps, 24);

arduino=serial('COM3','BaudRate',9600,'DataBits',8);    % create serial communication object on port COM3
InputBufferSize = 25;
Timeout = 0.1;
arduino.Terminator = '';
set(arduino , 'InputBufferSize', InputBufferSize);
set(arduino , 'Timeout', Timeout);
fopen(arduino); % initiate arduino communication


% Throw out first empty reading.
board.readChannels(1);

lastFallingEdgeIDX = -1;
lastFallingEdgeUsedIDX = -1;
for i = 1:steps
  beginningIndex = (i-1) * stepSize + 1;
  endingIndex = beginningIndex + stepSize- 1;
  
  reading = board.readChannels(stepSize);

  x(beginningIndex:endingIndex) = reading(:,1);

%   pause(stepSize/250);
  
  % Wait to get some data.
  if (i <= lookBack)
    continue
  end
 
  movingAverage = movmean(x(1:endingIndex), smoothingSamples);
  differences = vertcat(0, diff(movingAverage));
  
  if i == lookBack+1
      dev = std(abs(differences(1:endingIndex)));
      disp('Start')
  end

  % Checking if on rising edge of data (eye closing)
  isBlinkFallingEdge = differences < dev * -1.3;
  isBlinkRisingEdge  = differences > dev * 1.3;
  
  edgeChange = isBlinkFallingEdge * -1 + isBlinkRisingEdge;
  
  for sample = endingIndex-50:-1:(lookBack * stepSize) + 1
    if (all(edgeChange(sample-7:sample) == -1) && ...
        mean(edgeChange(sample+1:sample+50)) > .3)
      
      lastFallingEdgeIDX = sample;
      break
    end
  end
  
  if lastFallingEdgeUsedIDX + 5 < lastFallingEdgeIDX
    fprintf(arduino, '1>');
    disp('Blink at ' + string(lastFallingEdgeIDX))
    lastFallingEdgeUsedIDX = lastFallingEdgeIDX;
  end
end

fclose(arduino)
delete(board)

save x x