#include <Servo.h>

Servo servo;

const char endMarker = '>';

int direction = 1;

void setup() {
  Serial.begin(9600);  // initialize the serial port

  servo.attach(3);
  
  // set the speed at 60 rpm:
  pinMode(3, OUTPUT);

  delay(4);
  Serial.println('a');

}
void loop() {

  record();
  execute();
}

const byte numChar = 10;
char receivedChar[numChar];
bool newDataAvailable = false;

void record() {

  static byte ndx = 0;
  char   endMarker = '>';

  char rc;

  while (Serial.available() > 0 && newDataAvailable == false)
  {
    rc = Serial.read();
    delay(2);
    if (rc != endMarker)
    {
      receivedChar[ndx] = rc;
      ndx++;

      // if(ndx>=numChar)
      //   {
      //    ndx=numChar-1;
      //   }

    }
    else
    {
      receivedChar[ndx] = '\0'; //terminate the string
      ndx = 0;
      newDataAvailable = true;
    }

  }


  delay(1);

  //  if (newDataAvailable) {
  //      Serial.print(receivedChar);
  //
  //    newDataAvailable = false;
  //
  //  }


}

void execute() {

  if (newDataAvailable) {
    if (receivedChar[0] == '1') {
      direction *= -1;
//      Serial.println(direction);
    }

    if (direction == 1) {
      servo.write(180);
    } else {
      servo.write(0);
    }

    newDataAvailable = false;
  }



//
//    while (Serial.available() > 0) {
//      Serial.read();
//    }
}

