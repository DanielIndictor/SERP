import os.path

from ultimatetictactoe import UTTTBoard
from ultimatetictactoe import QAgent
from ultimatetictactoe import RandomAgent
from ultimatetictactoe import GreedyAgent


def single_game(agent_X, agent_O):
    board = UTTTBoard()

    current_agent, next_agent = (agent_X, UTTTBoard.PLAYER_X), \
                                (agent_O, UTTTBoard.PLAYER_O)

    while board.winner == UTTTBoard.NONE:
        current_agent[0].action(board, current_agent[1])
        current_agent, next_agent = next_agent, current_agent

    return board


def main():

    model_path = '/home/daniel/Documents/Code/SERP/SERP/Projects' \
                 '/ultimatetictactoe/main_model.HDF5'

    if os.path.isfile(model_path):
        q_agent = QAgent(model_path)
    else:
        q_agent = QAgent()

    opponent = RandomAgent()

    agent_x_wins = 0
    agent_o_wins = 0
    ties = 0

    games = 40000
    for x in range(games):

        print('Playing game', x)

        board = UTTTBoard()

        # Epsilon annealing
        epsilon = ((games-1) - x) * .82 / games + .02

        while True:
            q_agent.action(board, UTTTBoard.PLAYER_X,
                           epsilon=epsilon,
                           learn=False)
            if board.winner is not board.NONE:
                break
            opponent.action(board, UTTTBoard.PLAYER_O)
            if board.winner is not board.NONE:
                break

        if board.winner == UTTTBoard.PLAYER_X:
            agent_x_wins += 1
        elif board.winner == UTTTBoard.PLAYER_O:
            agent_o_wins += 1
        elif board.winner == UTTTBoard.TIE:
            ties += 1
        else:
            print(board.winner)
            print(board)

    print('Winner X', agent_x_wins)
    print('Winner O', agent_o_wins)
    print('Ties', ties)

    q_agent.save(model_path)

if __name__ == '__main__':
    main()
