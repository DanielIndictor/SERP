import numpy as np


class UTTTBoard:
    TIE = -2
    NONE = -1
    PLAYER_X = 0
    PLAYER_O = 1

    BOARD_SHAPE = [9, 9, 2]  # [Column, Row, Players]

    def __init__(self, board=None):
        if board is not None:
            self.state = np.copy(board.state)
            self._won_boxes = np.copy(board.won_boxes)
            self._valid_moves = np.copy(board.valid_moves)
            self._winner = board.winner
            return

        self.state = np.zeros(self.BOARD_SHAPE, dtype=np.int8)
        self._won_boxes = np.zeros([3, 3, 2], dtype=np.int8)
        self._valid_moves = np.ones([9, 9], dtype=np.int8)
        self._winner = self.NONE

    def __str__(self):
        string = ''

        def int_map(i):
            if i == 0:
                return ' '
            elif i == 1:
                return '⨯'  # Unicode
            else:
                return '○'  # Unicode

        single_channel_board = self.state[:, :, self.PLAYER_X] - \
            self.state[:, :, self.PLAYER_O]

        for row in range(11):
            if row == 3 or row == 7:
                string += '───┼───┼───\n'
                continue

            row_offset = 0
            if row > 3:
                row_offset += 1
                if row > 7:
                    row_offset += 1

            string += ''.join(map(int_map,
                                  single_channel_board[row - row_offset, 0:3]))
            string += '│'
            string += ''.join(map(int_map,
                                  single_channel_board[row - row_offset, 3:6]))
            string += '│'
            string += ''.join(map(int_map,
                                  single_channel_board[row - row_offset, 6:9]))

            string += '\n'

        return string

    @property
    def valid_moves(self):
        return self._valid_moves

    @valid_moves.setter
    def valid_moves(self, valid_moves):
        self._valid_moves = valid_moves

    @property
    def won_boxes(self):
        """ Returns a tensor of shape [3, 3, 2] representing which box each
        player won or lost in. """

        return self._won_boxes

    @won_boxes.setter
    def won_boxes(self, won_games):
        self.won_boxes = won_games

    @property
    def winner(self):
        return self._winner

    @winner.setter
    def winner(self, winner):
        self._winner = winner

    def move(self, row, column, player, long_term=False):
        """Returns reward of move for `player` at `[row, column]' and actually
         make the move on the board."""

        if self.valid_moves.sum() == 0:
            raise RuntimeError('No more moves left!', str(self))

        if self.valid_moves[row, column] == 0:
            raise RuntimeError('You can\'t move there!')

        self.state[row, column, player] = 1

        self.valid_moves[row, column] = 0

        sub_box_row = row // 3
        sub_box_column = column // 3

        if self.won_boxes[sub_box_row, sub_box_column, 0] == 1 or \
                self.won_boxes[sub_box_row, sub_box_column, 1] == 1:
            if self.valid_moves.sum() == 0:
                self.winner = self.TIE
            return np.float(-.001)

        box = self.state[3 * sub_box_row: 3 * sub_box_row + 3,
            3 * sub_box_column: 3 * sub_box_column + 3, player]

        box_won = False

        # Check if either of rows or columns (first line) have a match or if
        # any diagonals have a match (second line).
        if box.all(axis=0).any() or box.all(axis=1).any() or \
                np.all(box.diagonal()) or np.all(box[::-1, :].diagonal()):
            self.won_boxes[sub_box_row, sub_box_column, player] = 1
            box_won = True

        # Check if winning move
        if self.won_boxes.all(axis=0).any() or \
                self.won_boxes.all(axis=1).any() or \
                np.all(self.won_boxes.diagonal()) or \
                np.all(self.won_boxes[::-1, :].diagonal()):
            self.winner = player
            return 1
        elif box_won == False or not long_term:
            if self.valid_moves.sum() == 0:
                self.winner = self.TIE
            return -0.001
        else:
            if self.valid_moves.sum() == 0:
                self.winner = self.TIE
            return 0.2

    def rewards(self, player, long_term=False):
        """Returns expected rewards in shape (9,9) for a player.

        `UTTTBoard.rewards(UTTTBoard.PLAYER_O)[i, j]`
            returns the reward for player O to put an O at point (i,j).

        Rewards are as follows:
        Winning for `player`: +1
        Invalid move: -1
        If `long_term` is False, winning a small box will yield a reward of
        +0.2
        Tie: -0.5 reward
        Otherwise, a reward of -.001 is given.
        """

        rewards = np.zeros([9, 9])

        for row in range(9):
            for column in range(9):
                if self.valid_moves[row, column] == 0:
                    rewards[row, column] = -1
                else:
                    new_board = UTTTBoard(board=self)
                    rewards[row, column] = \
                        new_board.move(row, column, player,
                                       long_term=long_term)

        return rewards
