from itertools import permutations
from random import randint

import numpy as np

from .utttagent import UTTTAgent
from .utttboard import UTTTBoard


class RandomAgent(UTTTAgent):
    _POSSIBLE_ACTIONS = np.zeros([81, 2], dtype=np.int8)
    for x in np.arange(9):
        for y in np.arange(9):
            _POSSIBLE_ACTIONS[x * 9 + y] = [x, y]

    def action(self, board: UTTTBoard, player):

        intended_next_action_indices = np.random.permutation(81)

        for intended_action_index in intended_next_action_indices:
            intended_action = self._POSSIBLE_ACTIONS[intended_action_index]

            # If valid
            if board.valid_moves[intended_action[0], intended_action[1]] == 1:
                board.move(intended_action[0], intended_action[1], player)
                return

        raise RuntimeError('No possible moves left on board.\n' + str(self))





