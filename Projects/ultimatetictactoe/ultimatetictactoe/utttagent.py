from .utttboard import UTTTBoard


class UTTTAgent:
    def action(self, board, player):
        """Returns number from [0, 81) representing intended move."""
        raise NotImplementedError
