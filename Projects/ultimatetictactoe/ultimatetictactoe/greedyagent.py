from itertools import permutations
from random import randint

import numpy as np

from .utttagent import UTTTAgent
from .utttboard import UTTTBoard


class GreedyAgent(UTTTAgent):
    """Chooses action with immediate highest reward. If multiple actions have
    the same maximum reward, one is chosen at random."""
    def action(self, board: UTTTBoard, player):
        try:
            rewards = board.rewards(player=player)
        except RuntimeError:
            print(board)

        max_reward = rewards.max()

        maximum_indices = np.array(np.where(rewards == max_reward)).T

        chosen_move_number = randint(0, maximum_indices.shape[0] - 1)
        chosen_move_index = maximum_indices[chosen_move_number]

        board.move(chosen_move_index[0], chosen_move_index[1], player)





