import random

import numpy as np

from tensorflow import keras
from tensorflow.keras.models import load_model

from .utttagent import UTTTAgent
from .utttboard import UTTTBoard


class QAgent(UTTTAgent):
    def __init__(self, file_path=None, overwrite=True, include_optimizer=True):

        if file_path is not None:
            self.q_model = load_model(file_path)
            return

        self.q_model = keras.models.Sequential()
        self.q_model.add(keras.layers.Conv2D(100,
                                             3,
                                             padding='same',
                                             activation=keras.activations.relu,
                                             input_shape=(9, 9, 2)))
        self.q_model.add(keras.layers.Conv2D(100,
                                             3,
                                             padding='same',
                                             activation=keras.activations.relu
                                             ))
        self.q_model.add(keras.layers.Conv2D(100,
                                             3,
                                             padding='same',
                                             activation=keras.activations.relu
                                             ))
        self.q_model.add(keras.layers.Conv2D(100,
                                             3,
                                             padding='same',
                                             activation=keras.activations.relu
                                             ))
        self.q_model.add(keras.layers.Conv2D(100,
                                             3,
                                             padding='same',
                                             activation=keras.activations.relu
                                             ))
        self.q_model.add(keras.layers.Flatten())
        self.q_model.add(keras.layers.Dense(100,
                                            activation=keras.activations.relu))
        self.q_model.add(keras.layers.Dense(100,
                                            activation=keras.activations.relu))
        self.q_model.add(keras.layers.Dense(100,
                                            activation=keras.activations.relu))
        self.q_model.add(keras.layers.Dense(81))

        self.q_model.compile(optimizer=keras.optimizers.Adam(),
                             loss=keras.losses.mean_squared_error,
                             metrics=['accuracy'])

    def save(self, *args, **kwargs):
        self.q_model.save(*args, **kwargs)

    def action(self, board: UTTTBoard,
               player, epsilon=0,
               discount_factor=.95,
               learn=False):

        if player == 0:
            single_state = np.expand_dims(board.state, 0)
        else:  # Invert table so QAgent is the first channel.
            single_state = np.expand_dims(np.flip(board.state, 2), 0)

        q_values = self.q_model.predict(single_state)

        if random.random() < epsilon:
            intended_action = random.randint(0, 80)
            while True:
                if board.valid_moves[np.unravel_index(intended_action,
                                                      (9, 9))] == 1:
                    break
                else:
                    intended_action = random.randint(0, 80)

            action = intended_action
        else:
            enumerated_q_values = list([(index[0], value) for index, value in
                                        np.ndenumerate(q_values[0])])

            enumerated_q_values = np.array(enumerated_q_values,
                                           dtype=[('index', int),
                                                  ('q-value', np.float)])

            intended_actions = np.sort(enumerated_q_values,
                                       order=['q-value'],
                                       axis=0)

            for intended_pick in np.arange(81):
                intended_action = intended_actions[80 - intended_pick][0]
                if board.valid_moves[np.unravel_index(intended_action,
                                                      (9, 9))] == 1:
                    action = intended_action
                    break

        reward = board.move(*np.unravel_index(action, (9, 9)), player,
                            long_term=True)

        if not learn:
            return

        new_q_values = self.q_model.predict(single_state)
        new_q_max = np.max(new_q_values)

        y = q_values  # Here we assume that the q values for actions not chosen
        # don't need to be updated.
        y[0, action] = reward + discount_factor * new_q_max

        history = self.q_model.fit(single_state, y, verbose=0)

        print('\t' + str(history.history['loss'][0]))
